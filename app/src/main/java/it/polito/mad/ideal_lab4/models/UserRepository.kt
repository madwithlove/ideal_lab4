package it.polito.mad.ideal_lab4.models

import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.net.toUri
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.util.*

class UserRepository {
    var user: MutableLiveData<ProfileData> = MutableLiveData<ProfileData>()
    val db = FirebaseFirestore.getInstance()
    var items: MutableList<AdvItem> = mutableListOf()
    var interested = mutableListOf<ProfileData>()
    var interestedObservable: MutableLiveData<MutableList<ProfileData>> =
        MutableLiveData(interested)
    var itemsOfInterest: MutableList<AdvItem> = (mutableListOf())
    val observableItemsOfInterest: MutableLiveData<MutableList<AdvItem>> =
        MutableLiveData(itemsOfInterest)
    var userComments: MutableList<Comment> = (mutableListOf())
    val observableUserComments: MutableLiveData<MutableList<Comment>> =
        MutableLiveData(userComments)
    var itemsBought: MutableList<AdvItem> = (mutableListOf())
    val observableItemsBought: MutableLiveData<MutableList<AdvItem>> = MutableLiveData(itemsBought)


    val storage = Firebase.storage
    val storageReference = storage.reference


    private fun uploadPhoto(uri: Uri, id: String) {
        val riversRef = storageReference.child(id)
        val uploadTask = riversRef.putFile(uri)

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener {
            // Handle unsuccessful uploads
            Log.d("kkk", "upload foto fallito")
        }.addOnSuccessListener {
            // taskSnapshot.metadata contains file metadata such as size, content-type, etc.
            Log.d("kkk", "upload foto success")
        }
    }

    suspend fun loadPhoto(uri: Uri, id: String) {
        withContext(Dispatchers.IO) {
            val riversRef = storageReference.child(id)

            riversRef.getFile(uri)
        }
    }

    suspend fun getUser(userId: String, defaultProfile: ProfileData): ProfileData? =
        withContext(Dispatchers.IO)
        {
            // user.value = ProfileData()
            val result = getDbData(userId, defaultProfile)
            try {
                return@withContext result
                //return@withContext d
            } catch (error: Throwable) {
                return@withContext null
            }
        }

    suspend fun getInterestedUsers(itemId: String, userId: String): MutableList<ProfileData>? =
        withContext(Dispatchers.IO)
        {
            // user.value = ProfileData()
            val result = getDbInterested(itemId, userId)
            try {
                return@withContext interested
                //return@withContext d
            } catch (error: Throwable) {
                return@withContext null
            }
        }

    private fun getDbInterested(itemId: String, userId: String) {
        var result: MutableList<ProfileData> = mutableListOf()
        val job = db.collection("items")
            .document(itemId)
            .collection("interestedUsers")
            .get()
        Tasks.await(job)
        if (!job.isSuccessful)
            throw Exception()
        val snaps = job.result!!.documents
        for (d in snaps) {
            interested.add(d.toObject(ProfileData::class.java)!!)
        }
    }

    private fun getDbData(userId: String, defaultProfile: ProfileData): ProfileData {
        var us: ProfileData? = ProfileData()
        val f = db.collection("utenti").document(userId).get()
        Tasks.await(f)
        if (!f.isSuccessful)
            throw Exception()
        if (!f.result?.exists()!!) {
            createUser(userId, defaultProfile)
            return ProfileData()
        }
        val result = f.result!!.toObject(ProfileData::class.java)
        return result!!
    }

    suspend fun addComment(comment: Comment, userId: String, itemId: String) {
        withContext(Dispatchers.IO) {
            db.collection("utenti")
                .document(userId)
                .collection("comments")
                .document(itemId)
                .set(comment)
        }
        withContext(Dispatchers.IO) {
            val us = db.collection("utenti")
                .document(userId)

            val tt = db.runTransaction { transaction ->
                val snapshot = transaction.get(us)

                var mean: Double = snapshot.getDouble("rating")!!
                var num: Double = snapshot.getDouble("numOfRate")!!

                mean = (mean * num + comment.rating) / (num + 1)
                num = num + 1

                transaction.update(us, "rating", mean)
                transaction.update(us, "numOfRate", num)
            }
            Tasks.await(tt)

            //AGGIORNA VALUTAZIONE DELL'UTENTE NEGLI ALTRI RAMI DEL DB

            var user = db.collection("utenti")
                .document(userId).get()
            Tasks.await(user)
            if (user.isSuccessful) {
                var result = user.result!!
                var u = result.toObject(ProfileData::class.java)

                var items = db.collection("items")
                    .get()
                Tasks.await(items)
                var result2 = items.result!!
                for (i in result2) {
                    var it = i.toObject(AdvItem::class.java)
                    var t = db.collection("items")
                        .document(it.uuid)
                        .collection("interestedUsers")
                        .get()
                    Tasks.await(t)
                    val interested = t.result!!
                    for (us in interested!!) {
                        var uu = us.toObject(ProfileData::class.java)
                        if (uu.id == userId) {
                            db.collection("items")
                                .document(it.uuid)
                                .collection("interestedUsers")
                                .document(userId)
                                .set(u!!)
                        }
                    }

                }
            }


        }
    }

    suspend fun getUserItems(userId: String): MutableList<AdvItem>? =
        withContext(Dispatchers.IO) {
            try {
                val result = getUserItemsFromDb(userId)
                return@withContext items
            } catch (error: Throwable) {
                return@withContext null //temporaneo
            }
        }

    private fun getUserItemsFromDb(userId: String) {
        val db = FirebaseFirestore.getInstance()
        val coll = db.collection("utenti")
            .document(userId)
            .collection("userItem")
            .get()
        Tasks.await(coll)
        if (!coll.isSuccessful)
            throw Exception()
        val snaps = coll.result!!.documents
        for (d in snaps) {
            items.add(d.toObject(AdvItem::class.java)!!)
        }
    }


    private fun createUser(userId: String, defaultProfile: ProfileData) {
        db.collection("utenti")
            .document(userId)
            .set(defaultProfile)
            .addOnSuccessListener {

            }
            .addOnFailureListener {

            }
    }

    suspend fun removeItemPreference(userId: String, user: ProfileData, item: AdvItem) {
        withContext(Dispatchers.IO) {
            db.collection("items")
                .document(item.uuid)
                .collection("interestedUsers")
                .document(userId)
                .delete()
        }
        withContext(Dispatchers.IO) {
            db.collection("utenti")
                .document(userId)
                .collection("interests")
                .document(item.uuid)
                .delete()
        }
    }

    suspend fun getUserInterests(userId: String): MutableList<AdvItem>? =
        withContext(Dispatchers.IO) {
            try {
                getAllItemsOfInterestFromDb(userId)
                return@withContext itemsOfInterest
            } catch (error: Throwable) {
                return@withContext null
            }
        }

    suspend fun getUserComments(userId: String): MutableList<Comment>? =
        withContext(Dispatchers.IO) {
            try {
                getAllCommentsFromDb(userId)
                return@withContext userComments
            } catch (error: Throwable) {
                return@withContext null
            }
        }

    suspend fun getItemsBought(userId: String): MutableList<AdvItem>? =
        withContext(Dispatchers.IO) {
            try {
                getItemsBoughtFromDb(userId)
                return@withContext itemsBought
            } catch (error: Throwable) {
                return@withContext null
            }
        }


    private fun getItemsBoughtFromDb(userId: String) {
        var job = db.collection("utenti")
            .document(userId)
            .collection("buyItems")
            .get()
        Tasks.await(job)
        if (!job.isSuccessful)
            throw Exception()
        var result = job.result!!
        itemsBought.clear()
        for (i in result) {
            itemsBought.add(i.toObject(AdvItem::class.java))
        }

        db.collection("utenti")
            .document(userId)
            .collection("buyItems")
            .addSnapshotListener { value, e ->
                if (e == null) {
                    itemsBought.clear()
                    for (doc in value!!) {
                        itemsBought.add(doc.toObject(AdvItem::class.java))
                    }
                } else {
                    throw Exception()
                }
            }
    }


    suspend fun setItemBought(userId: String, item: AdvItem) {
        withContext(Dispatchers.IO) {
            db.collection("utenti")
                .document(userId)
                .collection("buyItems")
                .document(item.uuid)
                .set(item)
                .addOnFailureListener {
                    throw Exception()
                }
        }
        withContext(Dispatchers.IO) {
            val task = db.collection("utenti").get()
            Tasks.await(task)

            var list = task.result!!
            for (user in list) {

                var u = user.toObject(ProfileData::class.java)
                db.collection("utenti")
                    .document(u.id)
                    .collection("interests")
                    .document(item.uuid)
                    .delete()
            }

//            db.collection("utenti")
//                .document(userId)
//                .collection("interests")
//                .document(item.uuid)
//                .delete()
//                .addOnFailureListener {
//                    throw Exception()
//                }
        }

    }

    private fun getAllCommentsFromDb(userId: String) {
        val task = db.collection("utenti")
            .document(userId)
            .collection("comments")
            .get()
        Tasks.await(task)

        db.collection("utenti")
            .document(userId)
            .collection("comments")
            .addSnapshotListener { value, e ->
                if (e != null) {
                    // Error
                } else {
                    var updatedList = mutableListOf<Comment>()
                    updatedList = value!!.toObjects(Comment::class.java)
                    userComments.clear()
                    userComments.addAll(updatedList)
                }
            }

        if (!task.isSuccessful)
            throw Exception()
        val snaps = task.result!!
        userComments.clear()
        var listTemp = mutableListOf<Comment>()
        for (d in snaps) {
            userComments.add(d.toObject(Comment::class.java))
        }

    }


    @RequiresApi(Build.VERSION_CODES.N)
    private fun getAllItemsOfInterestFromDb(userId: String) {
        val task = db.collection("utenti")
            .document(userId)
            .collection("interests")
            .get()
        Tasks.await(task)

        db.collection("utenti")
            .document(userId)
            .collection("interests")
            .addSnapshotListener { value, e ->
                if (e != null) {
                    // Error
                } else {
                    var updatedList = mutableListOf<AdvItem>()
                    updatedList = value!!.toObjects(AdvItem::class.java)
                    for (j in updatedList) {
                        if (!j.sold) {
                            itemsOfInterest.removeIf { it.uuid == j.uuid }
                            itemsOfInterest.add(j)
                        }
                    }
                }
            }

        if (!task.isSuccessful)
            throw Exception()
        val snaps = task.result!!
        itemsOfInterest.clear()
        var listTemp = mutableListOf<AdvItem>()
        for (d in snaps) {
            var v = d.toObject(AdvItem::class.java)
            if (!v.sold) {
                itemsOfInterest.add(v)
                listTemp.add(d.toObject(AdvItem::class.java))
            }
        }


        var item = AdvItem()

        for (i in listTemp) {
            db.collection("items")
                .document(i.uuid)
                .get()
                .addOnSuccessListener {
                    item = it.toObject(AdvItem::class.java)!!

                    if (item.uuid == null)
                    else {
                        db.collection("utenti")
                            .document(userId)
                            .collection("interests")
                            .document(item.uuid)
                            .set(item)
                    }
                }

        }

    }

    suspend fun updateUser(id: String, user: ProfileData) {
        withContext(Dispatchers.IO) {
            db.collection("utenti")
                .document(id)
                .set(user)
                .addOnSuccessListener { Log.d("kkk", "update successfull!") }
                .addOnFailureListener { Log.d("kkk", "update failed!") }


            //modifica della collezione "items"
            var task = db.collection("items").get()
            Tasks.await(task)
            var list = task.result!!

            for (i in list) {
                var item = i.toObject(AdvItem::class.java)


                //aggiornamento user in interested user
                val t = db.collection("items")
                    .document(item.uuid)
                    .collection("interestedUsers")
                    .get()
                Tasks.await(t)
                val interested = t.result!!
                for (uu in interested) {
                    val u = uu.toObject(ProfileData::class.java)
                    if (u.id == id) {
                        db.collection("items")
                            .document(item.uuid)
                            .collection("interestedUsers")
                            .document(id)
                            .set(user)
                    }
                }
            }

            //aggiornamento fullname in item
            var resItem = db.collection("items")
                .whereEqualTo("creator_email", user.id)
                .get()
            Tasks.await(resItem)
            for (i in resItem.result!!) {

                var item = i.toObject(AdvItem::class.java)
                item.creator_fullname = user.fullName
                db.collection("items")
                    .document(item.uuid)
                    .set(item)
            }


            //modifica per gli user
            var resUser = db.collection("utenti").get()
            Tasks.await(resUser)
            for (uuu in resUser.result!!) {
                var utente = uuu.toObject(ProfileData::class.java)


                //modifica buyItems
                resItem = db.collection("utenti")
                    .document(utente.id)
                    .collection("buyItems")
                    .whereEqualTo("creator_email", user.id)
                    .get()
                Tasks.await(resItem)
                for (i in resItem.result!!) {
                    var item = i.toObject(AdvItem::class.java)
                    item.creator_fullname = user.fullName
                    db.collection("utenti")
                        .document(utente.id)
                        .collection("buyItems")
                        .document(item.uuid)
                        .set(item)

                }


                //modifica interests
                resItem = db.collection("utenti")
                    .document(utente.id)
                    .collection("interests")
                    .whereEqualTo("creator_email", user.id)
                    .get()
                Tasks.await(resItem)
                for (i in resItem.result!!) {
                    var item = i.toObject(AdvItem::class.java)
                    item.creator_fullname = user.fullName
                    db.collection("utenti")
                        .document(utente.id)
                        .collection("interests")
                        .document(item.uuid)
                        .set(item)
                }


                //modifica proprietario commenti
                var resComment = db.collection("utenti")
                    .document(utente.id)
                    .collection("comments")
                    .whereEqualTo("creatorEmail", user.id)
                    .get()
                Tasks.await(resComment)
                for (c in resComment.result!!){
                    var comment = c.toObject(Comment::class.java)
                    comment.creator=user.fullName
                    db.collection("utenti")
                        .document(utente.id)
                        .collection("comments")
                        .document(c.id)
                        .set(comment)

                }

            }

            withContext(Dispatchers.IO) {
                uploadPhoto(user.photo_uri.toUri(), id)
            }
        }

    }
}