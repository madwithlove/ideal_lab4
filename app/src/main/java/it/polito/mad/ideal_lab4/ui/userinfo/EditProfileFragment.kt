package it.polito.mad.ideal_lab4.ui.userinfo

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.provider.MediaStore
import android.view.*
import androidx.fragment.app.Fragment
import android.view.MenuInflater
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import it.polito.mad.ideal_lab4.models.ProfileData

import it.polito.mad.ideal_lab4.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.firebase.firestore.GeoPoint
import it.polito.mad.ideal_lab4.GpsUtils
import it.polito.mad.ideal_lab4.MainActivity
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel
import it.polito.mad.ideal_lab4.ui.GoogleMapsFragment
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*



class EditProfileFragment : Fragment() {
    lateinit var profile : MutableLiveData<ProfileData>
    private lateinit var model : UserViewModel
    private var mCurrentPhotoPath : String = "null"
    var photoURI : Uri? = null
    private val IMAGE_PICK_CODE = 1000
    val CAPTURE_IMAGE_REQUEST = 1
    val REQUEST_IMAGE_CAPTURE = 1
    var rotatedUri : Uri? = null
    private var lastClickTime : Long = 0
    lateinit var rotatedBitmap : Bitmap
    lateinit var rotatedPath : String
    val LOCATION_PERMISSION_REQUEST_CODE = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().toolbar.setTitle(R.string.title_profile_edit)
        var toolbar2 : Toolbar =  requireActivity().findViewById(R.id.toolbar2)
        toolbar2.visibility=View.GONE


        var nav: NavigationView = requireActivity().findViewById(R.id.nav_view)
        nav.menu.findItem(R.id.userShowFragment).setChecked(false)
        nav.menu.findItem(R.id.itemListFragment).setChecked(false)

        //maps button init
        //var mapsBtn : Button = requireActivity().findViewById(R.id.mapBtn)
        //var mapsBtn : EditText = requireActivity().findViewById(R.id.locationTxt)
        locationTxt.setOnClickListener{
            if (ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    GoogleMapsFragment.LOCATION_PERMISSION_REQUEST_CODE
                )

            }
            else {

                GpsUtils(requireContext()).turnGPSOn(object : GpsUtils.OnGpsListener {
                    override fun gpsStatus(isGPSEnable: Boolean) {
                        // turn on GPS
                        var isGPS = isGPSEnable
                        var b = Bundle()
                        b.putString("type", "Profile")
                        findNavController().navigate( R.id.googleMapsFragment,b)
                    }
                }, 3001)
            }
        }


        model = ViewModelProvider(requireActivity())
            .get(UserViewModel::class.java)

        var temp = model.getTemporaryPhotoUri()
        profile=model.getUserProfile()

        var myId = model.getUserId().value
        if(savedInstanceState == null) {
            profile.observe(viewLifecycleOwner, Observer {
                if (it.fullName != requireActivity().resources.getString(R.string.name))
                    nameTxt.setText(it.fullName)
                if (it.nick != requireActivity().resources.getString(R.string.nick))
                    nickTxt.setText(it.nick)
                if (it.email != requireActivity().resources.getString(R.string.email))
                    emailTxt.setText(it.email)
                if (model.getPositionAddr() != null){
                        locationTxt.setText(model.getPositionAddr())
                }
                else{
                    if(it.location != requireActivity().resources.getString(R.string.location))
                        locationTxt.setText(it.location)
                }


                (requireActivity() as MainActivity).getProfileGlide(
                    photo,
                    profile.value!!,
                    myId!!,
                    progressedit,
                    R.drawable.ic_user_icon
                )


            })
        }
        else{

            if(temp.value == "null") {
                (requireActivity() as MainActivity).getProfileGlide(
                    photo,
                    profile.value!!,
                    myId!!,
                    progressedit,
                    R.drawable.ic_user_icon
                )
            }
            else {
                Glide.with(requireActivity().applicationContext)
                    .load(Uri.parse(temp.value))
                    .apply(RequestOptions.circleCropTransform())
                    .into(photo)
                var progress : ProgressBar = requireActivity().findViewById(R.id.progressedit)
                progress.visibility=View.GONE
            }


        }


        //set fab
        var fab: FloatingActionButton = requireActivity().findViewById( R.id.fab )
        fab.hide()
        var fab2: ExtendedFloatingActionButton = requireActivity().findViewById( R.id.fab2 )
        fab2.hide()

        //hide search
        val search : Spinner = requireActivity().findViewById(R.id.search_spinner)
        search.visibility=View.GONE


        registerForContextMenu(editImage)
        editImage.setOnClickListener { it.showContextMenu() }

        rot.setOnClickListener {
            if (SystemClock.elapsedRealtime() - lastClickTime > 300) {
                rotateImage(Uri.parse(profile.value!!.photo_uri))
                lastClickTime = SystemClock.elapsedRealtime()
            }
        }

        // back navigation
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    model.setTemporaryPhotoUri("null")
                    model.setPosition(null)
                    model.setPositionAddr(null)
                    findNavController().navigate( R.id.userShowFragment )
                }
            }
        )

        // Up navigation
        requireActivity().toolbar.setNavigationOnClickListener {
            model.setTemporaryPhotoUri("null")
            model.setPosition(null)
            model.setPositionAddr(null)
            findNavController().navigate( R.id.userShowFragment )
        }
    }



    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.save, menu)
        super.onCreateOptionsMenu(menu, inflater)
        return
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.save -> {
                profile.value?.fullName=nameTxt.text.toString()
                profile.value?.nick=nickTxt.text.toString()
                profile.value?.email=emailTxt.text.toString()
                profile.value?.location=locationTxt.text.toString()
                if(model.getTemporaryPhotoUri().value!! != "null")
                    profile.value?.photo_uri=model.getTemporaryPhotoUri().value!!
                if(model.getPosition() != null)
                    profile.value!!.pos= GeoPoint(model.getPosition()!!.latitude, model.getPosition()!!.longitude)

                model.setPosition(null)
                model.setPositionAddr(null)
                model.setUser(profile.value!!)
                model.setTemporaryPhotoUri("null")
                findNavController().navigate(R.id.action_userEditorFragment_to_userShowFragment)

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        activity?.menuInflater?.inflate(R.menu.menu_image, menu)
    }


    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.gallery -> {
                openGallery()
                true
            }
            R.id.camera -> {
                captureImage()
                true
            }
            else ->
                super.onContextItemSelected(item)
        }
    }

    private fun captureImage() {

        if (ContextCompat.checkSelfPermission(requireActivity().applicationContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        } else {

            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(requireActivity().packageManager) != null) {
                // Create the File where the photo should go
                try {
                    //Creates a blank image file
                    var photoFile = createImageFile()

                    // Continue only if the File was successfully created
                    photoURI =
                        FileProvider.getUriForFile(requireActivity().applicationContext,
                            "it.polito.mad.ideal_lab4.fileprovider",
                            photoFile
                        )
                    //saving current photo_uri
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, CAPTURE_IMAGE_REQUEST)
                } catch (ex: Exception) {
                    // Error occurred while creating the File
                    displayMessage(requireActivity().baseContext,"Capture Image Bug: "  + ex.message.toString())
                }
            } else {
                displayMessage(requireActivity().baseContext, "Null")
            }
        }
    }

    private fun displayMessage(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".png",     /* suffix */
            storageDir          /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    private fun openGallery() {
        if (ContextCompat.checkSelfPermission(requireActivity().applicationContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        }
        else {
            val intent = Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            intent.type = "image/*"
            startActivityForResult(intent, IMAGE_PICK_CODE)
        }
    }

    fun rotateImage (imageUri: Uri) {
        val matrix = Matrix()

        var bitmap: Bitmap = photo.drawable.toBitmap()
        matrix.postRotate(90F)
        rotatedBitmap =
            Bitmap.createBitmap(bitmap, 0, 0, bitmap.height, bitmap.height, matrix, true)

        photo.setImageBitmap(rotatedBitmap)

        var rFile : File = createRotateFile()
        // Continue only if the File was successfully created
        photoURI = FileProvider.getUriForFile(requireContext(),
            "it.polito.mad.ideal_lab4.fileprovider",
            rFile
        )
        //Convert bitmap to byte array
        var bos: ByteArrayOutputStream = ByteArrayOutputStream();
        rotatedBitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
        var bitmapData: ByteArray = bos.toByteArray()


        //write the bytes in file
        var fos: FileOutputStream = FileOutputStream(rFile);
        fos.write(bitmapData);
        fos.flush();
        fos.close();
        profile.value!!.photo_uri = photoURI.toString()
        model.setTemporaryPhotoUri(photoURI.toString())
    }

    private fun createRotateFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "RotateFile_" + timeStamp + "_"
        val storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".png",     /* suffix */
            storageDir          /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAPTURE_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {  // image dalla camera
            profile.value!!.photo_uri = photoURI.toString()
            model.setTemporaryPhotoUri(photoURI.toString())

            Glide.with( requireActivity() ).load(Uri.parse(profile.value!!.photo_uri)).apply(RequestOptions.circleCropTransform()).into(photo)
        }
        else if(requestCode == IMAGE_PICK_CODE && resultCode == Activity.RESULT_OK) {  // image dalla gallery
            profile.value!!.photo_uri = data?.data.toString()
            model.setTemporaryPhotoUri(data?.data.toString())

            Glide.with(requireActivity()).load(Uri.parse(profile.value!!.photo_uri))
                .apply(RequestOptions.circleCropTransform()).into(photo)
        }
        else
            displayMessage(requireActivity().baseContext, getString(R.string.camera_error))
    }


}
