package it.polito.mad.ideal_lab4.ui.itemlist

import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import it.polito.mad.ideal_lab4.models.AdvAdapter
import it.polito.mad.ideal_lab4.models.AdvItem
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton

import it.polito.mad.ideal_lab4.R
import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_item_list.*
import java.util.*

class ItemListFragment : Fragment(){

    var bundleList = Bundle()
    private lateinit var itemArray : MutableLiveData<MutableList<AdvItem>>
    private lateinit var model : ItemViewModel
    private lateinit var userVM : UserViewModel
    private lateinit var adapter : AdvAdapter
    private lateinit var searchView : SearchView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        model = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        userVM = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        userVM.setLastFragment("ItemList")
        userVM.setLastFragmentEdit("item_list")

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //get items from db
        itemArray = userVM.getUserItems()
        model.setIsMyItem(true)
        model.setIsSoldToMe(false)

        //get NavController
        var nav: NavigationView = requireActivity().findViewById(R.id.nav_view)
        nav.setCheckedItem(R.id.itemListFragment)

        //set FAB
        var fab: FloatingActionButton = requireActivity().findViewById(R.id.fab)
        fab.show()
        fab.setOnClickListener {
            var b = Bundle()
            var find: Boolean = true
            var newId : String = ""
            while (find == true){
                newId = UUID.randomUUID().toString()
                find = itemArray.value!!.any { it.uuid == newId }
            }
            val newItem = AdvItem()
            newItem.uuid = newId
            completeNewItem(newItem)
            model.setCurrentItem(newItem)
            var bundle = Bundle()
            bundle.putString("frag", "item_list")
            findNavController().navigate(R.id.action_itemListFragment_to_itemEditFragment, bundle)
        }

        var fab2: ExtendedFloatingActionButton = requireActivity().findViewById(R.id.fab2)
        fab2.hide()

        //toolbar
        requireActivity().toolbar.setTitle(R.string.title_list)
        var toolbar2 : androidx.appcompat.widget.Toolbar =  requireActivity().findViewById(R.id.toolbar2)
        toolbar2.visibility=View.GONE

        // Up navigation
        requireActivity().toolbar.setNavigationOnClickListener {
            val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
            drawerLayout.openDrawer(GravityCompat.START)
        }

        //itemArray observer
        itemArray.observe(viewLifecycleOwner, Observer{
            ia ->
            if (itemArray.value!!.isEmpty()){
                msg.visibility = TextView.VISIBLE
            } else {
                msg.visibility = TextView.INVISIBLE
            }
            var recyclerView : RecyclerView = requireActivity().findViewById(R.id.recycler)
            adapter = AdvAdapter(itemArray.value!!,requireActivity() as AppCompatActivity)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(context)
        })

        // Up navigation
        requireActivity().toolbar.setNavigationOnClickListener {
            val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
            drawerLayout.openDrawer(GravityCompat.START)
        }

        // back navigation
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().navigate( R.id.action_itemListFragment_to_onSaleListFragment )
                }
            })
   }

    private fun manageSpinner(){
        val spinner : Spinner = requireActivity().findViewById(R.id.search_spinner)
        spinner.visibility=View.VISIBLE
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.search_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
        when (model.getselectedSearch()) {
            requireActivity().resources.getString(R.string.search1) -> spinner.setSelection(0)
            requireActivity().resources.getString(R.string.search2) -> spinner.setSelection(1)
            requireActivity().resources.getString(R.string.search3) -> spinner.setSelection(2)
            requireActivity().resources.getString(R.string.search4) -> spinner.setSelection(3)
            requireActivity().resources.getString(R.string.search6) -> spinner.setSelection(4)
            else -> spinner.setSelection(0)
        }
        spinner.onItemSelectedListener=object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when (p2) {
                    0 -> model.setselectedSearch(requireActivity().resources.getString(R.string.search1))
                    1 -> model.setselectedSearch(requireActivity().resources.getString(R.string.search2))
                    2 -> model.setselectedSearch(requireActivity().resources.getString(R.string.search3))
                    3 -> model.setselectedSearch(requireActivity().resources.getString(R.string.search4))
                    4 -> model.setselectedSearch(requireActivity().resources.getString(R.string.search6))
                    else -> model.setselectedSearch(requireActivity().resources.getString(R.string.search1))
                }
            }
        }
    }

    private fun completeNewItem(item : AdvItem){
        item.title = requireActivity().resources.getString(R.string.title)
        item.location = requireActivity().resources.getString(R.string.location)
        item.description = requireActivity().resources.getString(R.string.description)
        item.creator_email=userVM.getUserId().value!!
        item.creator_fullname=userVM.getFullname()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.main, menu)

        super.onCreateOptionsMenu(menu, inflater)
        return
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.action_search -> {
                var toolbar : Toolbar = requireActivity().findViewById(R.id.toolbar2)
                if(toolbar.isVisible){
                    item.setIcon(R.drawable.ic_arrow_down)
                    toolbar.visibility=View.GONE
                }
                else{
                    item.setIcon(R.drawable.ic_arrow_up)
                    toolbar.visibility=View.VISIBLE
                    val menu = toolbar.menu
                    //search spinner
                    manageSpinner()
                    val searchItem : MenuItem = menu.findItem(R.id.action_filter)
                    searchView  = searchItem.actionView as SearchView

                    searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
                        override fun onQueryTextSubmit(query: String?): Boolean {
                            return false
                        }

                        override fun onQueryTextChange(newText: String?): Boolean {
                            adapter.filter.filter(newText)
                            return false
                        }
                    })
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
