package it.polito.mad.ideal_lab4

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupActionBarWithNavController
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.storage.FirebaseStorage
import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel
import it.polito.mad.ideal_lab4.models.AdvItem
import it.polito.mad.ideal_lab4.models.ProfileData
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Text
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var mGoogleSignInClient : GoogleSignInClient
    private lateinit var auth: FirebaseAuth
    private lateinit var userVM : UserViewModel
    private lateinit var itemVM : ItemViewModel
    private var RC_SIGN_IN : Int = 1000
    private val RC_GPS_PROFILESHOW : Int = 3000
    private val RC_GPS_PROFILEEDIT : Int = 3001
    private val RC_GPS_ITEMSHOW : Int = 3002
    private val RC_GPS_ITEMEDIT : Int = 3003
    private var PERMISSION_REQUEST_CODE : Int = 10
    private var profileInfo : ProfileData = ProfileData()
    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    private val serverKey = "key=" + "AAAAR0QO2AM:APA91bHDDWRQnss_kBENO7lLygKazt8Y8bvL8aX1hNZ7nmh9URRxKhQafyOpg-kE9TMzvA9O4wZ8tFdoY-e64qLbcimOfucD3RfzHXWAvVMYxkbMBgwUecUn8xsAgmZwCcFVdDu0Pi5K"
    private val contentType = "application/json"
    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(applicationContext)
    }
    private lateinit var fullname_drawer: MutableLiveData<String>
    private lateinit var textView: TextView

    var accountIsNull : Boolean = false

    lateinit var gso : GoogleSignInOptions
    private lateinit var defaultProfile : ProfileData


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_main)
        //permissions
        if (ContextCompat.checkSelfPermission(
                applicationContext,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(
                applicationContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED

        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                PERMISSION_REQUEST_CODE
            )
        }




        auth = FirebaseAuth.getInstance()
        userVM = ViewModelProvider(this)
            .get(UserViewModel::class.java)
        itemVM = ViewModelProvider(this)
            .get(ItemViewModel::class.java)

        // Create default profile
        defaultProfile = ProfileData()
        setDefaultProfile(defaultProfile)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)


        setTab(false)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val toolbar2: Toolbar = findViewById(R.id.toolbar2)
        val navHeader = navView.getHeaderView(0)
        textView = navHeader.findViewById(R.id.textView)

        toolbar2.inflateMenu(R.menu.search_menu)
        toolbar2.visibility=View.GONE

        startService(Intent(this , MyService::class.java))


        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.itemListFragment, R.id.userShowFragment, R.id.onSaleListFragment, R.id.itemsOfInterestListFragment, R.id.boughtItemsListFragment), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setNavigationItemSelectedListener {
            drawerListener(it)
        }
        verifyAuthentication()

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED
                            && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {


                    // Permission is granted. Continue the action or workflow
                    // in your app.
                } else {
                val builder = AlertDialog.Builder(this)
                with(builder)
                {
                    setMessage(resources.getString(R.string.givePermissions))

                    setPositiveButton(
                        resources.getString(R.string.interested_yes),
                        DialogInterface.OnClickListener { _, _ ->
                            finish()
                        })

                    show()
                }


                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }



    fun drawerListener(it : MenuItem) : Boolean {
        when (it.itemId) {
            R.id.itemListFragment -> {
                    val host: NavHostFragment? = supportFragmentManager
                    .findFragmentById(R.id.nav_host_fragment) as NavHostFragment?

                val navController = host?.navController
                navController?.navigate(R.id.itemListFragment)
                userVM.setOtherUser(false)
                drawer_layout.closeDrawers()
                true
            }
            R.id.onSaleListFragment -> {
                val host: NavHostFragment? = supportFragmentManager
                    .findFragmentById(R.id.nav_host_fragment) as NavHostFragment?

                val navController = host?.navController
                navController?.navigate(R.id.onSaleListFragment)
                userVM.setOtherUser(false)
                drawer_layout.closeDrawers()
                true
            }
            R.id.boughtItemsListFragment -> {
                val host: NavHostFragment? = supportFragmentManager
                    .findFragmentById(R.id.nav_host_fragment) as NavHostFragment?

                val navController = host?.navController
                navController?.navigate(R.id.boughtItemsListFragment)
                userVM.setOtherUser(false)
                drawer_layout.closeDrawers()
                true
            }
            R.id.userShowFragment -> {
                val host: NavHostFragment? = supportFragmentManager
                    .findFragmentById(R.id.nav_host_fragment) as NavHostFragment?

                val navController = host?.navController
                navController?.navigate(R.id.userShowFragment)
                drawer_layout.closeDrawers()
                userVM.setOtherUser(false)
                true
            }
            R.id.itemsOfInterestListFragment -> {
                val host: NavHostFragment? = supportFragmentManager
                    .findFragmentById(R.id.nav_host_fragment) as NavHostFragment?

                val navController = host?.navController
                navController?.navigate(R.id.itemsOfInterestListFragment)
                drawer_layout.closeDrawers()
                userVM.setOtherUser(false)
                true
            }
            R.id.login -> {
                signIn()
                true
            }
            R.id.logout -> {
                signOut()
                return true
            }

            else -> false
        }
        return false
    }

    fun signIn(){
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    fun setItemBought(userId:String, itemId: AdvItem){
        try{
            userVM.setItemBought(userId, itemId)
        }
        catch(e:Exception){
            Toast.makeText(this, "An error occurred!", Toast.LENGTH_SHORT).show()
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun verifyAuthentication(){
        var gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        val account = GoogleSignIn.getLastSignedInAccount(applicationContext)



        //se account non è nullo allora siamo già loggati! il pulsante di login non deve essere mostrato
        //il controllo su myId serve a verificare se siamo già connessi a firebase. se è null vuol dire che ancora non siamo autenticati
        //in tutti gli altri casi vuol dire che abbiamo già autenticazione a firebase e dati
        //evita che vengano scaricati i dati 2 volte quando siamo già connessi
        if(account != null) {
            userVM.setUserId(account!!.email!!)
            userVM.setDefaultProfile(defaultProfile)
            userVM.getUserProfile()
            authenticateWithFirebase(account)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun authenticateWithFirebase(account : GoogleSignInAccount) {
        // Google Sign In successfull, now authenticate with the db
        drawer_layout.closeDrawers()
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Sign in success, show in drawer tab of logged user (my profile)
                    setTab(true)

                    userVM.setUserId(account!!.email!!)
                    //userVM.getUserInterests()
                    userVM.setDefaultProfile(defaultProfile)
                    userVM.getUserProfile()
                    fullname_drawer = userVM.getFullname_for_drawer()

                    // Observer for nickname to put in drawer
                    fullname_drawer.observe(this, Observer{
                        fd ->
                        if(fd == "" || fd == resources.getString(R.string.name) ){
                            textView.text = resources.getString(R.string.welcome) + "!"
                        }
                        else{
                            textView.text = resources.getString(R.string.welcome) + ", " + fullname_drawer.value + "!"
                        }

                    })

                    // subscribe
                    userVM.createTopic()
                    FirebaseMessaging.getInstance().subscribeToTopic( userVM.getTopic() )
                } else {
                    // If sign in fails, display a message to the user and logout from google
                    Toast.makeText(this,R.string.firebase_auth_failed, Toast.LENGTH_SHORT).show()
                    mGoogleSignInClient.signOut()
                    setTab(false)
                }
            }

    }

    fun sendMyNotification(userId : String, value : String, title : String){
        val notification = JSONObject()
        val notifcationBody = JSONObject()
        val topic : String = "/topics/"+userId.replace("@", "at", false)
        try {
            notifcationBody.put("title", title )
            notifcationBody.put("message", value)   //Enter your notification message
            notification.put("to", topic)
            notification.put("data", notifcationBody)
            Log.e("kkk-send", "try")
        } catch (e: JSONException) {
            Log.e("kkk-send", "send: " + e.message)
        }

        sendNotification(notification)

    }


    private fun sendNotification(notification: JSONObject) {
        Log.e("kkk-send", "sendNotification")
        val jsonObjectRequest =
            object : JsonObjectRequest(
                FCM_API,
                notification,
                Response.Listener<JSONObject> {
                        response -> Log.i("kkk-send", "onResponse: $response")
                },
                Response.ErrorListener {
                    Log.i("kkk-send", "onErrorResponse: Didn't work")
                } )
            {
                override fun getHeaders(): Map<String, String> {
                    val params = HashMap<String, String>()
                    params["Authorization"] = serverKey
                    params["Content-Type"] = contentType
                    return params
                }
            }
        requestQueue.add(jsonObjectRequest)
    }

    private fun setTab(value : Boolean){
        val menu = nav_view.menu
        menu.setGroupVisible(R.id.logged, value)
        menu.setGroupVisible(R.id.nonLogged, !value)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task: Task<GoogleSignInAccount> =
                GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                itemVM.getallItems(account!!.email!!)
                Snackbar.make(this.findViewById(R.id.drawer_layout), resources.getString(R.string.loginComplete), 2000).show()
                authenticateWithFirebase(account!!)
            }catch (e: ApiException) {
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Log.w("kkk", "signInResult:failed code=" + e.statusCode)
                Toast.makeText(applicationContext,"Login fallito! Riprova.", Toast.LENGTH_SHORT).show()
            }

        }
        else if(requestCode == RC_GPS_PROFILESHOW){
            val b = Bundle()
            b.putString("type", "GET_Profile")
            findNavController(R.id.nav_host_fragment).navigate( R.id.googleMapsFragment,b)

        }
        else if(requestCode == RC_GPS_PROFILEEDIT){
            var b = Bundle()
            b.putString("type", "Profile")
            findNavController(R.id.nav_host_fragment).navigate( R.id.googleMapsFragment,b)
        }
        else if(requestCode == RC_GPS_ITEMSHOW){
            var b = Bundle()
            b.putString("type", "GET_Item")
            findNavController(R.id.nav_host_fragment).navigate( R.id.googleMapsFragment,b)
        }
        else if(requestCode == RC_GPS_ITEMEDIT){
            var b = Bundle()
            b.putString("type", "Item")
            findNavController(R.id.nav_host_fragment).navigate( R.id.googleMapsFragment,b)
        }
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return NavigationUI.navigateUp(navController, drawer_layout) || super.onSupportNavigateUp()
    }

    private fun signOut(){
        mGoogleSignInClient.signOut()
        userVM.setUserId("null")
        findNavController(R.id.nav_host_fragment).navigate(R.id.onSaleListFragment)
        setTab(false)
        drawer_layout.closeDrawers()
        FirebaseMessaging.getInstance().unsubscribeFromTopic( userVM.getTopic() )
        FirebaseMessaging.getInstance().unsubscribeFromTopic( "mainaluca96atgmail.com")
        Snackbar.make(this.findViewById(R.id.drawer_layout), resources.getString(R.string.logoutComplete), Snackbar.LENGTH_SHORT).show()
        // Reset default message in drawer
        textView.text = resources.getString(R.string.nav_header_subtitle)
    }

    fun getCategory(idx : Int) : String {
        val list = resources.getStringArray( R.array.category_array )
        if (idx < list.size) {
            return list[idx]
        }
        return "Error"
    }

    fun getCategoryIndex(value : String) : Int {
        val list = resources.getStringArray( R.array.category_array )
        if (list.contains(value)) {
            return list.indexOf(value)
        }
        return -1
    }

    fun getSubCategory(idxCateg : Int, idxSub : Int) : String {
        var list = arrayOf<String>()
        when(idxCateg){
            0 ->
                list = resources.getStringArray(R.array.sub1)
            1 ->
                list = resources.getStringArray(R.array.sub2)
            2 ->
                list = resources.getStringArray(R.array.sub3)
            3 ->
                list = resources.getStringArray(R.array.sub4)
            4 ->
                list = resources.getStringArray(R.array.sub5)
            5 ->
                list = resources.getStringArray(R.array.sub6)
            6 ->
                list = resources.getStringArray(R.array.sub7)
            7 ->
                list = resources.getStringArray(R.array.sub8)
        }
        if (idxSub < list.size) {
            return list[idxSub]
        }
        return "Error"
    }

    fun getSubCategIdx (idxCateg : Int, subCateg : String) : Int {
        var list = arrayOf<String>()
        when(idxCateg){
            0 ->
                list = resources.getStringArray(R.array.sub1)
            1 ->
                list = resources.getStringArray(R.array.sub2)
            2 ->
                list = resources.getStringArray(R.array.sub3)
            3 ->
                list = resources.getStringArray(R.array.sub4)
            4 ->
                list = resources.getStringArray(R.array.sub5)
            5 ->
                list = resources.getStringArray(R.array.sub6)
            6 ->
                list = resources.getStringArray(R.array.sub7)
            7 ->
                list = resources.getStringArray(R.array.sub8)
        }
        if (list.contains(subCateg)) {
            return list.indexOf(subCateg)
        }
        return -1
    }

    private fun setDefaultProfile(defaultProfile: ProfileData){
        defaultProfile.fullName = resources.getString(R.string.name)
        defaultProfile.email = resources.getString(R.string.email)
        defaultProfile.nick = resources.getString(R.string.nick)
        defaultProfile.location = resources.getString(R.string.location)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    fun getImgGlide(view : ImageView, item : AdvItem, progress : ProgressBar, placeHolder : Int) {
        val storage =
            FirebaseStorage.getInstance()// Create a reference to a file from a Google Cloud Storage URI
        val gsReference =
            storage.getReferenceFromUrl("gs://manageadvfirestore.appspot.com/${item.uuid}")
        progress.visibility = View.VISIBLE
        Glide.with( applicationContext )
            .load(Uri.parse(item.photo_url))
            .listener(object : RequestListener<Drawable> {
                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progress.visibility = View.GONE
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            })
            .apply(RequestOptions.circleCropTransform())
            .error(
                GlideApp.with( applicationContext )
                    .load(gsReference)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .apply(RequestOptions.circleCropTransform())
                    .listener(object : RequestListener<Drawable> {

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            progress.visibility = View.GONE
                            return false
                        }

                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            progress.visibility = View.GONE
                            return false
                        }
                    })
                    .placeholder(placeHolder)
                    .error(placeHolder)
            )
            .placeholder( placeHolder )
            .into( view )
    }

    fun getProfileGlide(view : ImageView, user : ProfileData, userId : String, progress : ProgressBar, placeHolder : Int) {
        val storage =
            FirebaseStorage.getInstance()// Create a reference to a file from a Google Cloud Storage URI
        val gsReference =
            storage.getReferenceFromUrl("gs://manageadvfirestore.appspot.com/${userId}")
        progress.visibility = View.VISIBLE

        Glide.with( applicationContext )
            .load(Uri.parse(user.photo_uri))
            .listener(object : RequestListener<Drawable> {
                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progress.visibility = View.GONE
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    progress.visibility = View.GONE
                    return false
                }
            })
            .apply(RequestOptions.circleCropTransform())
            .error(
                GlideApp.with( applicationContext )
                    .load(gsReference)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .apply(RequestOptions.circleCropTransform())
                    .listener(object : RequestListener<Drawable> {

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            progress.visibility = View.GONE
                            return false
                        }

                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            progress.visibility = View.GONE
                            return false
                        }
                    })
                    .placeholder(placeHolder)
                    .error(placeHolder)
            )
            .placeholder( placeHolder )
            .into(view)
    }

}