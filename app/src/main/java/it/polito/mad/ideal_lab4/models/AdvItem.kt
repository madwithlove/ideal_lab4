package it.polito.mad.ideal_lab4.models

import android.os.Parcelable
import com.google.firebase.firestore.GeoPoint
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*



data class AdvItem(
    var uuid : String = UUID.randomUUID().toString(),
    var photo_url:String = "null",
    var title: String = "Titolo",
    var description:String= "Descrizione",
    var price : Double= 0.00,
    var category : String= "0",
    var subcategory : String = "0",
    var location : String= "Location",
    var creator_email : String ="null",
    var creator_fullname : String = "null",
    var available : Boolean = true,
    var sold : Boolean = false,
    var rating : Int = 0,
    var comment : String = "null",
    var geopoint : GeoPoint = GeoPoint(0.0,0.0),
    var date : String = SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Calendar.getInstance().time))

{
    override fun equals(other: Any?): Boolean {
        if(this.uuid.equals((other as AdvItem).uuid)
            && this.title.equals(other.title)
            && this.price.equals(other.price)
            && this.category.equals(other.category)
            && this.subcategory.equals(other.subcategory)
            && this.description.equals(other.description)
            && this.date.equals(other.date)
            && this.location.equals(other.location)
            && this.photo_url.equals(other.photo_url)
            && this.available.equals(other.available)
            && this.sold.equals(other.sold)
            && this.rating.equals(other.rating)
            && this.comment.equals(other.comment)
        )
            return true
        return false
    }

    override fun toString(): String {
        return "Category [photo_url: ${this.photo_url}, title: ${this.title}, description: ${this.description}, price: ${"%.2f".format(this.price)}, category: ${this.category}, location: ${this.location}, date: ${this.date}, available: ${this.available} ]"
    }
}