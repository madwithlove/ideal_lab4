package it.polito.mad.ideal_lab4.ui.userinfo

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.*
import android.widget.Button
import android.widget.RatingBar
import android.widget.Spinner
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import it.polito.mad.ideal_lab4.GpsUtils
import it.polito.mad.ideal_lab4.MainActivity
import it.polito.mad.ideal_lab4.R
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel
import it.polito.mad.ideal_lab4.models.ProfileData
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_show_profile.*


class ShowProfileFragment : Fragment() {

    private lateinit var myId : MutableLiveData<String>
    private lateinit var user : LiveData<ProfileData>
    private lateinit var isOtherProfile : MutableLiveData<Boolean>
    private lateinit var mGoogleSignInClient : GoogleSignInClient
    private lateinit var auth: FirebaseAuth
    private var RC_SIGN_IN : Int = 1000
    private lateinit var model: UserViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        /* */
        model = ViewModelProvider(requireActivity())
            .get(UserViewModel::class.java)
        myId = model.getUserId()
        auth = FirebaseAuth.getInstance()


        /* toolbar name */
        requireActivity().toolbar.setTitle(R.string.title_myProfile_show)
        var toolbar2 : Toolbar =  requireActivity().findViewById(R.id.toolbar2)
        toolbar2.visibility=View.GONE

        return inflater.inflate(R.layout.fragment_show_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isOtherProfile = model.getIsOtherUser()
        arguments?.clear()

        isOtherProfile.observe(viewLifecycleOwner, Observer { b ->
            if (!b) {
                showMyProfile()
            } else {
                showOtherProfile()
            }
        })


        //hide search
        val search: Spinner = requireActivity().findViewById(R.id.search_spinner)
        search.visibility = View.GONE

        //hide FAB
        var fab: FloatingActionButton = requireActivity().findViewById(R.id.fab)
        fab.hide()
        var fab2: ExtendedFloatingActionButton = requireActivity().findViewById(R.id.fab2)
        fab2.hide()


        backNav()

// Up navigation
        requireActivity().toolbar.setNavigationOnClickListener {
            val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
            drawerLayout.openDrawer(GravityCompat.START)
            //model.setOtherUser(false)
        }
    }

    fun backNav() {
        if (isOtherProfile.value == true) {

            // back navigation
            requireActivity().onBackPressedDispatcher.addCallback(
                viewLifecycleOwner,
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        var start = model.getLastFragment2()
                        if(start == "ItemDetails")
                            findNavController().navigate(R.id.itemDetailsFragment)
                        else
                            findNavController().navigate(R.id.interestedUsersFragment)
                        model.setOtherUser(false)
                    }
                }
            )
        } else {

            // back navigation
            requireActivity().onBackPressedDispatcher.addCallback(
                viewLifecycleOwner,
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        findNavController().navigate(R.id.action_userShowFragment_to_onSaleListFragment)
                        model.setOtherUser(false)
                    }
                }
            )
        }
    }

    fun showOtherProfile(){

        var toolbar: Toolbar = requireActivity().findViewById(R.id.toolbar)
        toolbar.setTitle(R.string.title_otherProfile_show)

        user = model.getLastVisited()
        // cfr con getUserProfile

        user.observe(viewLifecycleOwner, Observer{
                u ->
            nameTxt.text = if(u.fullName == "null") requireActivity().resources.getString(R.string.name) else u.fullName
            emailTxt.text = if(u.email == "null") requireActivity().resources.getString(R.string.email) else u.email
            nickTxt.text = if(u.nick == "null") requireActivity().resources.getString(R.string.nick) else u.nick
            locationTxt.text = if(u.location == "null") requireActivity().resources.getString(R.string.location) else u.location

            manageRating()

            (requireActivity() as MainActivity).getProfileGlide( photo, user.value!!, user.value!!.id, progressshow, R.drawable.ic_user_icon )

            //controllo posizione inserita
            var locationTextView: TextView = requireActivity().findViewById(R.id.locationTxt)
            if(user.value!!.pos.latitude != 0.0 && user.value!!.pos.longitude != 0.0){
                locationTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark))
                var list = locationTextView.compoundDrawables
                for(d : Drawable in list){
                    if(d != null) {
                        d.colorFilter = PorterDuffColorFilter(
                            ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark)
                            , PorterDuff.Mode.SRC_IN)
                    }
                }
                locationTextView.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorCard))
                locationTextView.isEnabled = true
                locationTextView.setOnClickListener {
                    GpsUtils(requireContext()).turnGPSOn(object : GpsUtils.OnGpsListener {
                        override fun gpsStatus(isGPSEnable: Boolean) {
                            // turn on GPS
                            var isGPS = isGPSEnable
                            var b = Bundle()
                            b.putString("type", "GET_Profile")
                            findNavController().navigate( R.id.googleMapsFragment,b)
                        }
                    }, 3000)
                }
            }
            else{
                locationTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.grayDisabled))
                locationTextView.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.cardGrey))
                var list = locationTextView.compoundDrawables
                for(d : Drawable in list){
                    if(d != null) {
                        d.colorFilter = PorterDuffColorFilter(
                            ContextCompat.getColor(requireContext(), R.color.grayDisabled)
                            , PorterDuff.Mode.SRC_IN)
                    }
                }
                locationTextView.isEnabled = false
            }

            //gestione commenti btn
            var commentBtn : Button = requireActivity().findViewById(R.id.commentBtn)
            if(user!!.value!!.rating!! < 1){
                commentBtn.visibility=View.GONE
            }
            commentBtn.setOnClickListener{
                findNavController().navigate(R.id.commentListFragment)
            }



        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        if(!model.getIsOtherUser().value!!) {
            inflater.inflate(R.menu.edit, menu)
            super.onCreateOptionsMenu(menu, inflater)
            return
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.edit -> {
                editProfile()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun editProfile(){
        val bundle = Bundle()
        val gson = Gson().toJson(user.value)
        bundle.putString("Profile",gson)
        findNavController().navigate(R.id.action_userShowFragment_to_userEditorFragment, bundle)
    }


    private fun showMyProfile(){

        var toolbar: Toolbar = requireActivity().findViewById(R.id.toolbar)
        toolbar.setTitle(R.string.title_myProfile_show)
        user = model.getUserProfile()

        var nav: NavigationView = requireActivity().findViewById(R.id.nav_view)
        nav.setCheckedItem(R.id.userShowFragment)


        user.observe(viewLifecycleOwner, Observer{
                u ->
            nameTxt.text = if(u.fullName == "null") requireActivity().resources.getString(R.string.name) else u.fullName
            emailTxt.text = if(u.email == "null") requireActivity().resources.getString(R.string.email) else u.email
            nickTxt.text = if(u.nick == "null") requireActivity().resources.getString(R.string.nick) else u.nick
            locationTxt.text = if(u.location == "null") requireActivity().resources.getString(R.string.location) else u.location

            manageRating()

            (requireActivity() as MainActivity).getProfileGlide( photo, u, myId.value!!, progressshow, R.drawable.ic_user_icon )

            //controllo posizione inserita
            var locationTextView: TextView = requireActivity().findViewById(R.id.locationTxt)
            if(user.value!!.pos.latitude != 0.0 && user.value!!.pos.longitude != 0.0){
                locationTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark))
                var list = locationTextView.compoundDrawables
                for(d : Drawable in list){
                    if(d != null) {
                        d.colorFilter = PorterDuffColorFilter(
                            ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark)
                            , PorterDuff.Mode.SRC_IN)
                    }
                }
                locationTextView.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorCard))
                locationTextView.isEnabled = true
                locationTextView.setOnClickListener {
                    GpsUtils(requireContext()).turnGPSOn(object : GpsUtils.OnGpsListener {
                        override fun gpsStatus(isGPSEnable: Boolean) {
                            // turn on GPS
                            var isGPS = isGPSEnable
                            var b = Bundle()
                            b.putString("type", "GET_Profile")
                            findNavController().navigate( R.id.googleMapsFragment,b)
                        }
                    }, 3000)
                }
            }
            else{
                locationTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.grayDisabled))
                locationTextView.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.cardGrey))
                var list = locationTextView.compoundDrawables
                for(d : Drawable in list){
                    if(d != null) {
                        d.colorFilter = PorterDuffColorFilter(
                            ContextCompat.getColor(requireContext(), R.color.grayDisabled)
                            , PorterDuff.Mode.SRC_IN)
                    }
                }
                locationTextView.isEnabled = false
            }

            //gestione commenti btn
            var commentBtn : Button = requireActivity().findViewById(R.id.commentBtn)
            if(user!!.value!!.rating!! < 1){
                commentBtn.visibility=View.GONE
            }
            commentBtn.setOnClickListener{
                findNavController().navigate(R.id.commentListFragment)
            }
        })




        var gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), gso);
        val account = GoogleSignIn.getLastSignedInAccount(requireContext())

        //se account non è nullo allora siamo già loggati! il pulsante di login non deve essere mostrato
        //il controllo su myId serve a verificare se siamo già connessi a firebase. se è null vuol dire che ancora non siamo autenticati
        //in tutti gli altri casi vuol dire che abbiamo già autenticazione a firebase e dati
        //evita che vengano scaricati i dati 2 volte quando siamo già connessi
        if(account != null && myId.value == "null") {
            //authenticateWithFirebase(account)
            //model.setUserId(account.email!!)
        }


    }


    fun manageRating(){

        var ratingBar: RatingBar = requireActivity().findViewById(R.id.ratingBar)
        var ratingTxt: TextView = requireActivity().findViewById(R.id.ratingTxt)
        if(user.value!!.rating!! >= 1){
            ratingBar.setRating(user!!.value!!.rating)
            ratingBar.setIsIndicator(true)
            var ratingTxt_toFormat: String = String.format("%.1f", user!!.value!!.rating)
            val spannable: Spannable = SpannableString(ratingTxt_toFormat)
            spannable.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.colorAccent)),
                0,
                ratingTxt_toFormat.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            spannable.setSpan(
                StyleSpan(Typeface.BOLD),
                0,
                ratingTxt_toFormat.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            spannable.setSpan(
                RelativeSizeSpan(1.5f),
                0,
                ratingTxt_toFormat.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            ratingBar.visibility = View.VISIBLE
            ratingTxt.visibility = View.VISIBLE
            var noRatingsTxt: TextView = requireActivity().findViewById(R.id.noRatingsTxt)
            noRatingsTxt.visibility = View.GONE
            ratingTxt.setText(spannable, TextView.BufferType.SPANNABLE)
        } else {
            ratingBar.visibility = View.INVISIBLE
            ratingTxt.visibility = View.GONE
            var noRatingsTxt: TextView = requireActivity().findViewById(R.id.noRatingsTxt)
            noRatingsTxt.visibility = View.VISIBLE
        }

    }



}
