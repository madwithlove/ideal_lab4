package it.polito.mad.ideal_lab4

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.IntentSender
import android.location.LocationManager
import android.util.Log
import android.widget.Toast
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*

class GpsUtils() {
    lateinit var context: Context
    var GPS_REQUEST: Int = 3000
    var isGPS: Boolean = false

    constructor(context: Context) : this() {
        this.context = context
        var mSettingsClient: SettingsClient? = null
        var mLocationSettingsRequest: LocationSettingsRequest? = null
        var locationManager: LocationManager? = null
        var locationRequest: LocationRequest? = null


        locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        mSettingsClient =
            LocationServices.getSettingsClient(context)
        locationRequest = LocationRequest.create()
        locationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        locationRequest!!.interval = 10 * 1000.toLong()
        locationRequest!!.fastestInterval = 2 * 1000.toLong()
        val builder: LocationSettingsRequest.Builder =
            LocationSettingsRequest.Builder().addLocationRequest(locationRequest!!)
        mLocationSettingsRequest = builder.build()

        builder.setAlwaysShow(true); //this is the key ingredien
    }

    public interface OnGpsListener {
        fun gpsStatus(isGPSEnable: Boolean);
    }


    fun turnGPSOn(onGpsListener: GpsUtils.OnGpsListener, requestCode : Int) {
        var locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var mSettingsClient =
            LocationServices.getSettingsClient(context)
        var locationRequest = LocationRequest.create()
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.interval = 10 * 1000.toLong()
        locationRequest.fastestInterval = 2 * 1000.toLong()
        val builder: LocationSettingsRequest.Builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
        var mLocationSettingsRequest = builder.build()


        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (onGpsListener != null) {
                onGpsListener.gpsStatus(true)
            }
        } else {
            mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(
                    context as Activity
                ) { //  GPS is already enabled, callback GPS status through listener
                    if (onGpsListener != null) {
                        onGpsListener.gpsStatus(true)
                    }
                }
                .addOnFailureListener(
                    context as Activity
                ) { e ->
                    val statusCode: Int = (e as ApiException).getStatusCode()
                    when (statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                            // Show the dialog by calling startResolutionForResult(), and check the
                            // result in onActivityResult().
                            val rae: ResolvableApiException = e as ResolvableApiException
                            rae.startResolutionForResult(
                                context as Activity,
                                requestCode
                            )
                        } catch (sie: IntentSender.SendIntentException) {
                            Log.d(ContentValues.TAG, "PendingIntent unable to execute request.")
                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            val errorMessage =
                                "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings."
                            Log.e(ContentValues.TAG, errorMessage)
                            Toast.makeText(
                                context as Activity?,
                                errorMessage,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                }
        }
    }
}

