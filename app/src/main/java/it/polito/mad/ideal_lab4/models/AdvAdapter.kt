package it.polito.mad.ideal_lab4.models
import android.widget.ProgressBar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import it.polito.mad.ideal_lab4.R
import android.widget.Filter
import it.polito.mad.ideal_lab4.MainActivity
import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel

class AdvAdapter(var data: MutableList<AdvItem>, var act : AppCompatActivity) : RecyclerView.Adapter<AdvAdapter.AdvViewHolder>(), Filterable  {
    var exampleListFull : MutableList<AdvItem> = mutableListOf()
    init {
        exampleListFull.addAll(data)
    }
    var viewModel : ItemViewModel = ViewModelProvider(act).get(ItemViewModel::class.java)

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdvViewHolder {

        val v : View
        if(viewModel.getIsMyItem()){
            v = LayoutInflater.from(parent.context).inflate(R.layout.advitem_small, parent,false)
        }
        else{
            v = LayoutInflater.from(parent.context).inflate(R.layout.advitem_small_no_edit, parent,false)
        }

        return AdvViewHolder(v, act)
    }


    override fun onBindViewHolder(holder: AdvViewHolder, position: Int) {
        if(position < data.size) {
            val u = data[position]
            holder.bind(u)
        }
    }


    class AdvViewHolder(v: View, act: AppCompatActivity):RecyclerView.ViewHolder(v) {
        val photo: ImageView = v.findViewById(R.id.photo)
        val title: TextView = v.findViewById(R.id.title)
        val price: TextView = v.findViewById(R.id.price)

        val date: TextView = v.findViewById(R.id.date)

        val location: TextView = v.findViewById(R.id.location)
        val card: CardView = v.findViewById(R.id.card)

        val progressbar: ProgressBar = v.findViewById(R.id.progress_circular)
        val mNav = act.findNavController(R.id.nav_host_fragment)
        val b: Bundle = Bundle()
        val act = act
        var model = ViewModelProvider(act).get(ItemViewModel::class.java)
        var userVM = ViewModelProvider(act).get(UserViewModel::class.java)
        var view = v


        fun bind(a: AdvItem) {
            title.text = a.title.toString()
            price.text = "%.2f".format(a.price)
            date.text = a.date.toString()
            location.text = a.location.toString()

            if(model.getIsMyItem()) {

                if (a.sold) {
                    val btnEdit: Button = view.findViewById(R.id.btnEdit)
                    btnEdit.visibility = View.INVISIBLE
                } else {
                    val btnEdit: Button = view.findViewById(R.id.btnEdit)
                    btnEdit.visibility = View.VISIBLE
                }
            }

            if (!a.available) {
                title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0)
            }

                (act as MainActivity).getImgGlide(photo, a, progressbar, R.drawable.ic_empty_photo)


                card.setOnClickListener {
                    model.setCurrentItem(a)
                    mNav.navigate(R.id.itemDetailsFragment)
                }


                if (model.getIsMyItem()) {
                    val btnEdit: Button = view.findViewById(R.id.btnEdit)
                    btnEdit.setOnClickListener {
                        userVM.setLastFragmentEdit("item_list")
                        model.setCurrentItem(a)
                        mNav.navigate(R.id.action_itemListFragment_to_itemEditFragment)
                    }
                }


        }
    }

    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraints: CharSequence?): FilterResults {
                var filteredList : MutableList<AdvItem> = mutableListOf()
                if(constraints == null || constraints.isEmpty()){
                    filteredList.addAll(exampleListFull)
                }else{
                    when(viewModel.getselectedSearch()){
                        act.resources.getString(R.string.search1)->filteredList = filterByTitle(constraints)
                        act.resources.getString(R.string.search2)->filteredList = filterByMinPrice(constraints)
                        act.resources.getString(R.string.search3)->filteredList = filterByMaxPrice(constraints)
                        act.resources.getString(R.string.search4)->filteredList = filterByCategory(constraints)
                        act.resources.getString(R.string.search6)->filteredList = filterByLocation(constraints)
                    }
                }
                var results : FilterResults = FilterResults()
                results.values = filteredList
                return results
            }

            override fun publishResults(constraints: CharSequence?, results: FilterResults?) {
                data.clear()
                data.addAll(results!!.values as MutableList<AdvItem>)
                notifyDataSetChanged()
            }


            private fun filterByTitle(constraints : CharSequence) : MutableList<AdvItem>{
                var filterPattern : String = constraints.toString().toLowerCase().trim()
                var list = mutableListOf<AdvItem>()
                for(a : AdvItem in exampleListFull){
                    if(a.title.toLowerCase().contains(filterPattern)){
                        list.add(a)
                    }
                }
                return list
            }

            private fun filterByCategory(constraints : CharSequence) : MutableList<AdvItem>{
                var filterPattern : String = constraints.toString().toLowerCase().trim()
                var list = mutableListOf<AdvItem>()
                for(a : AdvItem in exampleListFull){
                    var category = getCategory(a.category.toInt())
                    if(category.toLowerCase().contains(filterPattern)){
                        list.add(a)
                    }
                }
                return list
            }

            fun getCategory(idx : Int) : String {
                val list = act.resources.getStringArray( R.array.category_array )
                return list[idx]
            }



            private fun filterByLocation(constraints : CharSequence) : MutableList<AdvItem>{
                var filterPattern : String = constraints.toString().toLowerCase().trim()
                var list = mutableListOf<AdvItem>()
                for(a : AdvItem in exampleListFull){
                    if(a.location.toLowerCase().contains(filterPattern)){
                        list.add(a)
                    }
                }
                return list
            }

            private fun filterByMinPrice(constraints : CharSequence) : MutableList<AdvItem>{
                var list = mutableListOf<AdvItem>()
                var min = constraints.toString().toIntOrNull()
                if(min == null)
                    return list

                for(a : AdvItem in exampleListFull){
                    if(a.price >= min){
                        list.add(a)
                    }
                }
                return list
            }

            private fun filterByMaxPrice(constraints : CharSequence) : MutableList<AdvItem>{
                var list = mutableListOf<AdvItem>()
                var max = constraints.toString().toIntOrNull()
                if(max == null)
                    return list

                for(a : AdvItem in exampleListFull){
                    if(a.price <= max){
                        list.add(a)
                    }
                }
                return list
            }


        }
    }



}