package it.polito.mad.ideal_lab4.models

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import it.polito.mad.ideal_lab4.R
import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class CommentAdapter(var data: MutableList<Comment>, var act : AppCompatActivity) : RecyclerView.Adapter<CommentAdapter.CommentViewHolder>(){

    var userVM : UserViewModel = ViewModelProvider(act).get(UserViewModel::class.java)

    override fun getItemCount() = data.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.comment_small, parent,false)

        return CommentViewHolder(v, act, this)
    }


    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val u = data[position]
        holder.bind(u)
    }


    class CommentViewHolder(v: View, act: AppCompatActivity, adapter : CommentAdapter): RecyclerView.ViewHolder(v) {
        val ratingBar : RatingBar = v.findViewById(R.id.ratingBar)
        val autore : TextView = v.findViewById(R.id.autore)
        val comment : TextView = v.findViewById(R.id.comment)
        val date : TextView = v.findViewById(R.id.date)
        var expand : Boolean = false

        val card: CardView = v.findViewById(R.id.card)

        val mNav = act.findNavController(R.id.nav_host_fragment)

        val act = act
        var userVM: UserViewModel = ViewModelProvider(act).get(UserViewModel::class.java)
        var itemVM: ItemViewModel = ViewModelProvider(act).get(ItemViewModel::class.java)
        var view = v
        var adapter = adapter


        fun bind(a: Comment) {
            ratingBar.rating=a.rating.toFloat()
            autore.text=a.creator
            comment.text=a.comment

            val format2: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val finalDay: String = format2.format(a.date)

            date.text=finalDay


            card.setOnClickListener {
                if (expand == false) {
                    expand = true
                    comment.isSingleLine = false
                } else {
                    expand = false
                    comment.maxLines = 3
                    comment.ellipsize = TextUtils.TruncateAt.END
                }
            }
        }
    }
}


