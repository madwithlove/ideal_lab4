package it.polito.mad.ideal_lab4.ui.itemdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import it.polito.mad.ideal_lab4.R
import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel
import it.polito.mad.ideal_lab4.models.AdvItem
import it.polito.mad.ideal_lab4.models.ProfileData
import it.polito.mad.ideal_lab4.models.UserAdapter
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_interested_users.*


class InterestedUsersFragment() : Fragment(){
    constructor(item : AdvItem) : this(){
        this.item = item
    }

    private lateinit var interestedArray : MutableLiveData<MutableList<ProfileData>>
    private lateinit var userVM : UserViewModel
    private lateinit var itemVM : ItemViewModel
    private lateinit var adapter : UserAdapter
    private lateinit var item : AdvItem


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_interested_users, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        userVM = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        itemVM = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        item= itemVM.getCurItem().value!!

        //set fab
        var fab : FloatingActionButton =  requireActivity().findViewById(R.id.fab)
        fab.hide()
        var fab2 : ExtendedFloatingActionButton =  requireActivity().findViewById(R.id.fab2)
        fab2.hide()


        //set toolbar
        requireActivity().toolbar.setTitle(R.string.title_interested_users)
        var toolbar2 : Toolbar =  requireActivity().findViewById(R.id.toolbar2)
        toolbar2.visibility=View.GONE

        interestedArray = userVM.getInterestedUsers(item.uuid)

        interestedArray.observe(viewLifecycleOwner, Observer{

                ia ->
            //show interested users
            if(interestedArray.value!!.isEmpty()) {
                nothing_interestedUsers.setText(R.string.no_users)
                nothing_interestedUsers.setTextSize(22F)
            }
            else
                nothing_interestedUsers.setText("")
            var recyclerView : RecyclerView = requireActivity().findViewById(R.id.recyclerView)
            adapter = UserAdapter(interestedArray.value!!,requireActivity() as AppCompatActivity)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(context)
        })

        // Up navigation
        requireActivity().toolbar.setNavigationOnClickListener {
            findNavController().navigate( R.id.action_interestedUsersFragment_to_itemDetailsFragment )
        }

        // Back navigation
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().navigate( R.id.action_interestedUsersFragment_to_itemDetailsFragment )
                }
            }
        )

    }


}