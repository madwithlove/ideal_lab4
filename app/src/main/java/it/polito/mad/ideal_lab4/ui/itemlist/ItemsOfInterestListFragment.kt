package it.polito.mad.ideal_lab4.ui.itemlist

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView

import it.polito.mad.ideal_lab4.R
import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel
import it.polito.mad.ideal_lab4.models.AdvAdapter
import it.polito.mad.ideal_lab4.models.AdvItem
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_onsaleitems.*

class ItemsOfInterestListFragment : Fragment() {
    var bundleList = Bundle()
    private lateinit var itemsOfInterest : MutableLiveData<MutableList<AdvItem>>
    private lateinit var model : ItemViewModel
    private lateinit var userVM : UserViewModel
    private lateinit var adapter : AdvAdapter
    private lateinit var searchView : SearchView
    private lateinit var userId : MutableLiveData<String>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        model = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        userVM = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        userVM.setLastFragment("Interests")

        userId=userVM.getUserId()


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_items_of_interest_list, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //get items from db
        itemsOfInterest = userVM.getUserInterests()
        //model.setIsMyItem(false)
        model.setIsSoldToMe(false)
        model.setIsMyItem(false)

        //get NavController
        var nav: NavigationView = requireActivity().findViewById(R.id.nav_view)
        nav.setCheckedItem(R.id.itemsOfInterestListFragment)

        //set FAB
        var fab: FloatingActionButton = requireActivity().findViewById(R.id.fab)
        fab.hide()


        var fab2: ExtendedFloatingActionButton = requireActivity().findViewById(R.id.fab2)
        fab2.hide()

        //toolbar
        requireActivity().toolbar.setTitle(R.string.title_itemsOfInterest)
        var toolbar2 : Toolbar =  requireActivity().findViewById(R.id.toolbar2)
        toolbar2.visibility = View.GONE

        // Up navigation
        requireActivity().toolbar.setNavigationOnClickListener {
            val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
            drawerLayout.openDrawer(GravityCompat.START)
        }

        itemsOfInterest.observe(viewLifecycleOwner, Observer{
                ia ->
            if (itemsOfInterest.value!!.isEmpty()){
                msg.visibility = TextView.VISIBLE
            } else {
                msg.visibility = TextView.INVISIBLE
            }
            var recyclerView : RecyclerView = requireActivity().findViewById(R.id.recycler)
            adapter = AdvAdapter(itemsOfInterest.value!!,requireActivity() as AppCompatActivity)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(context)
        })


        // back navigation
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {


                    findNavController().navigate( R.id.onSaleListFragment )
                    //findNavController().popBackStack(R.id.action_itemsOfInterestListFragment_to_onSaleListFragment, true)
                }
            })

    }

    private fun manageSpinner(){
        val spinner : Spinner = requireActivity().findViewById(R.id.search_spinner)
        spinner.visibility= View.VISIBLE
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.search_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
        when (model.getselectedSearch()) {
            requireActivity().resources.getString(R.string.search1) -> spinner.setSelection(0)
            requireActivity().resources.getString(R.string.search2) -> spinner.setSelection(1)
            requireActivity().resources.getString(R.string.search3) -> spinner.setSelection(2)
            requireActivity().resources.getString(R.string.search4) -> spinner.setSelection(3)
            requireActivity().resources.getString(R.string.search6) -> spinner.setSelection(4)
            else -> spinner.setSelection(0)
        }
        spinner.onItemSelectedListener=object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when (p2) {
                    0 -> model.setselectedSearch(requireActivity().resources.getString(R.string.search1))
                    1 -> model.setselectedSearch(requireActivity().resources.getString(R.string.search2))
                    2 -> model.setselectedSearch(requireActivity().resources.getString(R.string.search3))
                    3 -> model.setselectedSearch(requireActivity().resources.getString(R.string.search4))
                    4 -> model.setselectedSearch(requireActivity().resources.getString(R.string.search6))
                    else -> model.setselectedSearch(requireActivity().resources.getString(R.string.search1))
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main, menu)

        super.onCreateOptionsMenu(menu, inflater)
        return
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.action_search -> {
                var toolbar : Toolbar = requireActivity().findViewById(R.id.toolbar2)
                if(toolbar.isVisible) {
                    item.setIcon(R.drawable.ic_arrow_down)
                    toolbar.visibility = View.GONE
                }
                else{
                    item.setIcon(R.drawable.ic_arrow_up)
                    toolbar.visibility=View.VISIBLE
                    val menu = toolbar.menu
                    manageSpinner()
                    val searchItem : MenuItem = menu.findItem(R.id.action_filter)
                    searchView  = searchItem.actionView as SearchView

                    searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
                        override fun onQueryTextSubmit(query: String?): Boolean {
                            return false
                        }

                        override fun onQueryTextChange(newText: String?): Boolean {
                            adapter.filter.filter(newText)
                            return false
                        }
                    })
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
