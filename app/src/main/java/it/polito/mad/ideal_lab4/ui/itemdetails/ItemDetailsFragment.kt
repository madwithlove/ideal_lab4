package it.polito.mad.ideal_lab4.ui.itemdetails


import android.app.AlertDialog
import android.content.DialogInterface
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import it.polito.mad.ideal_lab4.GpsUtils
import it.polito.mad.ideal_lab4.MainActivity
import it.polito.mad.ideal_lab4.R
import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel
import it.polito.mad.ideal_lab4.models.AdvItem
import it.polito.mad.ideal_lab4.models.ProfileData
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_item_details.*


class ItemDetailsFragment : Fragment() {
    var item : MutableLiveData<AdvItem> = MutableLiveData(AdvItem())
    private var available : Boolean = false
    private lateinit var model : ItemViewModel
    private lateinit var userVM : UserViewModel
    private var isMyItem : Boolean = false
    private var isSoldToMe: Boolean = false
    private lateinit var interestedItems : MutableLiveData<MutableList<AdvItem>>
    private lateinit var totItems : MutableLiveData<MutableList<AdvItem>>
    //private lateinit var fragment : InterestedUsersFragment
    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    private val serverKey = "key=" + "AAAAR0QO2AM:APA91bHDDWRQnss_kBENO7lLygKazt8Y8bvL8aX1hNZ7nmh9URRxKhQafyOpg-kE9TMzvA9O4wZ8tFdoY-e64qLbcimOfucD3RfzHXWAvVMYxkbMBgwUecUn8xsAgmZwCcFVdDu0Pi5K"
    private val contentType = "application/json"
    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(requireContext())
    }
    private var fragShown = false

    enum class fabState {LIKED, UNLIKED, BLOCKED, HIDE}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        model = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        userVM = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)

        interestedItems = userVM.getUserInterests()
        totItems= model.getallItems(userVM.getUserId().value!!)
        item = model.getCurItem()


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_item_details, container, false)
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        //set toolbar
        requireActivity().toolbar.setTitle(R.string.title_item_details)
        var toolbar2 : Toolbar =  requireActivity().findViewById(R.id.toolbar2)
        toolbar2.visibility=View.GONE

        //set NavigationView
        var nav: NavigationView = requireActivity().findViewById(R.id.nav_view)
        nav.menu.findItem(R.id.userShowFragment).setChecked(false)
        nav.menu.findItem(R.id.itemListFragment).setChecked(false)

        var soldText: TextView = requireActivity().findViewById( R.id.soldView )
        if(model.getCurItem().value!!.sold)
            soldText.visibility=View.VISIBLE
        else
            soldText.visibility=View.GONE

        isMyItem = model.getIsMyItem()
        if(isMyItem)
            showMyItem()
        else
            showOtherItem()

        setAdvItem(item.value!!)

        // Manage FABs
        var fab: FloatingActionButton = requireActivity().findViewById( R.id.fab )
        fab.hide()


        //hide search
        val search : Spinner = requireActivity().findViewById(R.id.search_spinner)
        search.visibility=View.GONE


        // Up navigation
        requireActivity().toolbar.setNavigationOnClickListener {
            when(userVM.getLastFragment()){
                "Home"-> findNavController().navigate(R.id.onSaleListFragment)
                "ItemList" ->findNavController().navigate(R.id.itemListFragment)
                "Interests" -> findNavController().navigate(R.id.itemsOfInterestListFragment)
                "Bought" -> findNavController().navigate(R.id.boughtItemsListFragment)
                else -> findNavController().navigate(R.id.onSaleListFragment)
            }
        }

        // Back navigation
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    when(userVM.getLastFragment()){
                        "Home"-> findNavController().navigate(R.id.onSaleListFragment)
                        "ItemList" ->findNavController().navigate(R.id.itemListFragment)
                        "Interests" -> findNavController().navigate(R.id.itemsOfInterestListFragment)
                        "Bought" -> findNavController().navigate(R.id.boughtItemsListFragment)
                        else -> findNavController().navigate(R.id.onSaleListFragment)
                    }

                }
            }
        )
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun showOtherItem(){
        var btn: Button = requireActivity().findViewById( R.id.showFragBtn )
        btn.visibility=View.GONE

        var owner : TextView = requireActivity().findViewById(R.id.owner)
        owner.visibility = View.VISIBLE
        if(item.value!!.creator_fullname.isNullOrEmpty() || item.value!!.creator_fullname == requireActivity().resources.getString(R.string.name))
            owner.setText(requireActivity().resources.getString(R.string.undefined_username_adapter))
        else
            owner.setText(item.value!!.creator_fullname)

        item.observe(viewLifecycleOwner, Observer{
                i ->
            available = i.available

            //controllo posizione inserita
            var locationTextView: TextView = requireActivity().findViewById(R.id.location)
            locationTextView.setOnClickListener {
                GpsUtils(requireContext()).turnGPSOn(object : GpsUtils.OnGpsListener {
                    override fun gpsStatus(isGPSEnable: Boolean) {
                        // turn on GPS
                        var b = Bundle()
                        b.putString("type", "GET_Item")
                        findNavController().navigate( R.id.googleMapsFragment,b)
                    }
                }, 3002)
            }
            if(item.value!!.geopoint.latitude != 0.0 && item.value!!.geopoint.longitude != 0.0){
                locationTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark))
                var drawableList = locationTextView.compoundDrawables
                for(d : Drawable in drawableList){
                    if(d != null) {
                        d.colorFilter = PorterDuffColorFilter(
                            ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark)
                            , PorterDuff.Mode.SRC_IN)
                    }
                }
                locationTextView.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorCard))
                locationTextView.isEnabled = true
            }
            else{
                locationTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.grayDisabled))
                locationTextView.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.cardGrey))
                var drawableList = locationTextView.compoundDrawables
                for(d : Drawable in drawableList){
                    if(d != null) {
                        d.colorFilter = PorterDuffColorFilter(
                            ContextCompat.getColor(requireContext(), R.color.grayDisabled)
                            , PorterDuff.Mode.SRC_IN)
                    }
                }
                locationTextView.isEnabled = false
            }

        } )

        //navigazione verso profilo creator
        val ownerLink : TextView = requireActivity().findViewById(R.id.owner)
        ownerLink.setOnClickListener{
            userVM.setOtherUser(true)
            val u = ProfileData()
            u.id = item.value!!.creator_email
            userVM.setLastVisited(u)
            userVM.setLastFragment2("ItemDetails")
            findNavController().navigate(R.id.userShowFragment)
        }

        var fab2: ExtendedFloatingActionButton = requireActivity().findViewById( R.id.fab2 )
        if(item.value!!.sold)
            fab2.hide()
        else{
            if(userVM.getUserId().value != "null") {

                setFab(fabState.UNLIKED)

                fab2.setOnClickListener {
                    if(fab2.text == requireActivity().resources.getString(R.string.interested_do)){
                        //LASCI IL GRADIMENTO VERSO L'OGGETTO
                        val builder = AlertDialog.Builder(requireContext())
                        with(builder)
                        {
                            setTitle(requireActivity().getString(R.string.interested_confirm1))
                            setMessage(requireActivity().getString(R.string.interested_confirm2))

                            setPositiveButton(
                                requireActivity().resources.getString(R.string.interested_yes),
                                DialogInterface.OnClickListener {
                                        _, _ ->
                                    snackbarEmail()
                                } )
                            setNegativeButton(
                                requireActivity().resources.getString(R.string.interested_no),
                                DialogInterface.OnClickListener { _, _ ->
                                } )
                            show()
                        }
                    }
                    else{ // clicco not interessed
                        snackbarEmail()
                    }
                }

                //così ho sempre l'item aggiornato e so se è bloccato
                totItems.observe(viewLifecycleOwner, Observer{
                        i ->
                    var found = false
                    for(i : AdvItem in totItems.value!!){
                        if(i.uuid == item.value!!.uuid){
                            found = true
                            item.value = i
                        }
                    }
                })

                interestedItems.observe(viewLifecycleOwner, Observer {
                        i ->
                    var found = false
                    for(i : AdvItem in interestedItems.value!!){
                        if(i.uuid == item.value!!.uuid){
                            found = true
                        }
                    }
                    //cambiare qui il tipo di fab
                    var avail = item.value!!.available
                    if(item.value!!.sold)
                    {
                        var fab2: ExtendedFloatingActionButton = requireActivity().findViewById( R.id.fab2 )
                        fab2.hide()
                    }
                    else {
                        if (avail) {
                            if (found == true) {
                                setFab(fabState.LIKED)
                            } else {
                                setFab(fabState.UNLIKED)
                            }
                        } else {
                            setFab(fabState.BLOCKED)
                        }
                    }
                })
            }
        }
        //controllo per oggetto venduto a me
        isSoldToMe=model.getIsSoldToMe()
        var rateBtn: Button = requireActivity().findViewById(R.id.RateUserBtn)
        if(isSoldToMe){
            rateBtn.visibility=View.VISIBLE
            rateBtn.setOnClickListener {

                var tempProf : ProfileData = ProfileData(item.value!!.creator_email, item.value!!.creator_fullname, "null")
                userVM.setLastVisited(tempProf)
                findNavController().navigate( R.id.action_itemDetailsFragment_to_userRateFragment)
            }
            if(model.getCurItem().value!!.rating != 0)
                rateBtn.isEnabled = false

        }
        else{
            rateBtn.visibility=View.GONE
        }




    }

    private fun showMyItem(){

        var owner : TextView = requireActivity().findViewById(R.id.owner)
        owner.visibility = View.GONE



        item.observe(viewLifecycleOwner, Observer{
                i ->

            //controllo posizione inserita
            var locationTextView: TextView = requireActivity().findViewById(R.id.location)
            locationTextView.setOnClickListener {
                GpsUtils(requireContext()).turnGPSOn(object : GpsUtils.OnGpsListener {
                    override fun gpsStatus(isGPSEnable: Boolean) {
                        // turn on GPS
                        var b = Bundle()
                        b.putString("type", "GET_Item")
                        findNavController().navigate( R.id.googleMapsFragment,b)
                    }
                }, 3002)
            }

            if(item.value!!.geopoint.latitude != 0.0 && item.value!!.geopoint.longitude != 0.0){
                locationTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark))
                var list = locationTextView.compoundDrawables
                for(d : Drawable in list){
                    if(d != null) {
                        d.colorFilter = PorterDuffColorFilter(
                            ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark)
                            , PorterDuff.Mode.SRC_IN)
                    }
                }
                locationTextView.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorCard))
                locationTextView.isEnabled = true
            }
            else{
                locationTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.grayDisabled))
                locationTextView.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.cardGrey))
                var list = locationTextView.compoundDrawables
                for(d : Drawable in list){
                    if(d != null) {
                        d.colorFilter = PorterDuffColorFilter(
                            ContextCompat.getColor(requireContext(), R.color.grayDisabled)
                            , PorterDuff.Mode.SRC_IN)
                    }
                }
                locationTextView.isEnabled = false
            }

        } )

        if(userVM.getUserId().value != "null") {
            var fab2: ExtendedFloatingActionButton = requireActivity().findViewById( R.id.fab2 )
            fab2.hide()

            var text : TextView = requireActivity().findViewById(R.id.soldView)
            text.visibility=View.VISIBLE
            //set item status
            if(item.value!!.sold)
            {
                text.text = requireActivity().resources.getString(R.string.itemSold)
                text.setTextColor(ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.itemSold)))
            }
            else if(!item.value!!.available){
                text.text = requireActivity().resources.getString(R.string.itemBlocked)
                text.setTextColor(ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.itemBlocked)))
            }
            else{
                text.text = requireActivity().resources.getString(R.string.itemAvailable)
                text.setTextColor(ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.itemAvailable)))
            }

            var btn: Button = requireActivity().findViewById(R.id.showFragBtn)
            btn.setOnClickListener {
                findNavController().navigate( R.id.action_itemDetailsFragment_to_interestedUsersFragment )
            }
        }
    }

    private fun snackbarEmail(){
        // Invia notifica
        var fab2: ExtendedFloatingActionButton = requireActivity().findViewById( R.id.fab2 )
        if(fab2.text==requireActivity().resources.getString(R.string.interested_do)) {
            if (available) {
                model.addItemPreference(
                    userVM.getUserId().value!!,
                    userVM.getUserProfile().value!!,
                    item.value!!
                )
                setFab( fabState.LIKED)
                if(userVM.getFullname()!=""){
                    (requireActivity() as MainActivity).sendMyNotification(item.value!!.creator_email, userVM.getFullname()+
                            " "+requireActivity().resources.getString(R.string.notifica_body)+" "+item.value!!.title, requireActivity().resources.getString(R.string.notifica_title))
                } else {
                    (requireActivity() as MainActivity).sendMyNotification(item.value!!.creator_email, requireActivity().resources.getString(R.string.undefined_username)+
                            " "+requireActivity().resources.getString(R.string.notifica_body)+" "+item.value!!.title, requireActivity().resources.getString(R.string.notifica_title))
                }
            } else {
                Toast.makeText(requireContext(), requireActivity().resources.getString(R.string.item_not_available), Toast.LENGTH_LONG).show()
                setFab( fabState.BLOCKED )
            }
        }
        else{
            userVM.removeItemPreference(
                userVM.getUserId().value!!,
                userVM.getUserProfile().value!!,
                item.value!!
            )
            if(available)
                setFab( fabState.UNLIKED )
            else
            {
                Toast.makeText(requireContext(), requireActivity().resources.getString(R.string.item_not_available), Toast.LENGTH_LONG).show()
                setFab( fabState.BLOCKED )
            }

        }
    }


    private fun setAdvItem(item: AdvItem){

            title.text = item.title
            description.text = item.description
            price.text = "%.2f".format(item.price)
            location.text = item.location
            date.text = item.date
        val act =(activity as MainActivity)
            category.text = act.getCategory(item.category.toInt()) + " - " + act.getSubCategory(item.category.toInt(), item.subcategory.toInt())

        val progress = requireActivity().findViewById<ProgressBar>(R.id.detailsprogressbar)

        (requireActivity() as MainActivity).getImgGlide( photo, item, progress, R.drawable.ic_empty_photo )
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        if(isMyItem && !item.value!!.sold)
            inflater.inflate(R.menu.edit_adv_item, menu)
        super.onCreateOptionsMenu(menu, inflater)
        return
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.edit -> {
                // chiamo edit fragment
                model.setCurrentItem(this.item.value!!)
                userVM.setLastFragmentEdit("item_details")

                findNavController().navigate( R.id.action_itemDetailsFragment_to_itemEditFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun setFab(type : fabState) {
        val fab2 = requireActivity().findViewById<ExtendedFloatingActionButton>( R.id.fab2)
        when(type){
            fabState.HIDE -> fab2.hide()
            fabState.UNLIKED -> {
                fab2.text=requireActivity().resources.getString(R.string.interested_do)
                fab2.icon=ContextCompat.getDrawable(requireContext(), R.drawable.ic_heart_outlined)
                fab2.backgroundTintList=(ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorAccent)));
                fab2.isClickable=true
                fab2.show()
            }
            fabState.LIKED -> {
                fab2.text=requireActivity().resources.getString(R.string.interested_undo)
                fab2.icon=ContextCompat.getDrawable(requireContext(), R.drawable.ic_heart_filled)
                fab2.backgroundTintList=(ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorAccent)));
                fab2.isClickable=true
                fab2.show()
            }
            fabState.BLOCKED -> {
                fab2.text=requireActivity().resources.getString(R.string.interested_unavailable)
                fab2.backgroundTintList=(ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.grayDisabled)));
                fab2.isClickable=false
                fab2.icon=ContextCompat.getDrawable(requireContext(), R.drawable.ic_unavailable)
                fab2.show()
            }
        }
    }
}



