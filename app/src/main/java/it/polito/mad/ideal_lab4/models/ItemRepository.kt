package it.polito.mad.ideal_lab4.models

import android.net.Uri
import android.util.Log
import androidx.core.net.toUri
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class ItemRepository {
    var items : MutableList<AdvItem> = mutableListOf()
    val observableItems : MutableLiveData<MutableList<AdvItem>> = MutableLiveData(items)
    var item : AdvItem = AdvItem()
    val obsItem : MutableLiveData<AdvItem> = MutableLiveData(item)
    val db = FirebaseFirestore.getInstance()
    val storage = Firebase.storage
    val storageReference = storage.reference

    suspend fun getAllItems(userId : String) : MutableList<AdvItem>? =
        withContext(Dispatchers.IO) {
            try{
                val result = getItemsFromDb(userId)
                return@withContext items
            }
            catch(error:Throwable){
                return@withContext null
            }
        }

    suspend fun getCurItem(uuid:String) : AdvItem? =
        withContext(Dispatchers.IO){
            try{
                val result = getCurItemFromDb(uuid)
                return@withContext item
            }
            catch(error:Throwable){
                item = AdvItem()
                return@withContext item
            }
        }

    private fun getCurItemFromDb(uuid: String){
        val db = FirebaseFirestore.getInstance()
        val coll = db.collection("items")
            .document(uuid).get()
        Tasks.await(coll)
        if (!coll.isSuccessful)
            throw Exception()
        val snaps = coll.result!!.toObject(AdvItem::class.java)
        item=snaps!!


        val t = db.collection("items")
            .document(uuid)
            .addSnapshotListener { value, e ->
                if (e != null) { //Handle error
                } else {
                        var obj = value!!.toObject(AdvItem::class.java)!!
                        obsItem.value = obj
                }
            }


    }



    private fun getItemsFromDb(userId : String) {
        items.clear()
        val db = FirebaseFirestore.getInstance()
        val coll = db.collection("items").get()
        Tasks.await(coll)
        if (!coll.isSuccessful)
            throw Exception()
        val snaps = coll.result!!.documents


        if(userId == "null") {
            for (d in snaps) {
                var dObject = d.toObject(AdvItem::class.java)!!
                if(!dObject.sold) {
                    items.add(d.toObject(AdvItem::class.java)!!)
                }
            }
        }
        else{
            for (d in snaps) {
                var obj = d.toObject(AdvItem::class.java)!!
                if(obj.creator_email != userId  && !obj.sold) {
                    items.add(obj)
                }
            }
        }

        val t = db.collection("items")
            .addSnapshotListener { value, e ->
                if (e != null) { //Handle error
                } else {
                    items.clear()
                    if(userId == "null") {
                        for (doc in value!!) {
                            var dObject = doc.toObject(AdvItem::class.java)!!
                            if(!dObject.sold) {
                                items.add(doc.toObject(AdvItem::class.java))
                            }
                        }
                    }
                    else{
                        for (doc in value!!) {
                            var obj = doc.toObject(AdvItem::class.java)!!
                            if(obj.creator_email != userId && !obj.sold)
                                items.add(doc.toObject(AdvItem::class.java))
                        }
                    }
                }
            }
    }

    suspend fun addItem(item : AdvItem, userId : String) {
        withContext(Dispatchers.IO) {

            val db = FirebaseFirestore.getInstance()
            val task = db.collection("items")
                .document(item.uuid)
                .set(item)
            //inserire l'item nell'array di items dello user
            db.collection("utenti")
                .document(userId)
                .collection("userItem")
                .document(item.uuid)
                .set(item)

            Tasks.await(task)
            return@withContext task.isSuccessful
        }
        withContext(Dispatchers.IO) {
            uploadPhoto( item.photo_url.toUri(), item.uuid)
        }
    }

    private fun uploadPhoto(uri : Uri, userId: String){
        val riversRef = storageReference.child(userId)
        val uploadTask = riversRef.putFile(uri)

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener {
            // Handle unsuccessful uploads
            Log.d("kkk", "upload foto fallito")
        }.addOnSuccessListener {
            // taskSnapshot.metadata contains file metadata such as size, content-type, etc.
            Log.d("kkk", "upload foto success")
        }
    }


    suspend fun addItemPreference(interestedId : String, interestedUser : ProfileData, item: AdvItem){
        withContext(Dispatchers.IO){
            db.collection("items")
                .document(item.uuid)
                .collection("interestedUsers")
                .document(interestedId)
                .set(interestedUser)
        }
        withContext(Dispatchers.IO){
            db.collection("utenti")
                .document(interestedId)
                .collection("interests")
                .document(item.uuid)
                .set(item)
        }
    }


}