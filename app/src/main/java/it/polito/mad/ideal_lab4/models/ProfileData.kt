package it.polito.mad.ideal_lab4.models

import com.google.firebase.firestore.GeoPoint


data class ProfileData  ( var id: String = "null",
                          var fullName: String = "null",
                          var email: String = "null",
                          var nick: String = "null",
                          var location: String = "null",
                          var photo_uri: String = "null",
                          var rating : Float = 0F,
                          var numOfRate: Int = 0,
                          var pos :  GeoPoint = GeoPoint(0.0, 0.0)
                           ) {
}