package it.polito.mad.ideal_lab4.ui.userinfo

import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import it.polito.mad.ideal_lab4.R
import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel
import it.polito.mad.ideal_lab4.models.AdvAdapter
import it.polito.mad.ideal_lab4.models.AdvItem
import it.polito.mad.ideal_lab4.models.Comment
import it.polito.mad.ideal_lab4.models.CommentAdapter
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_comment_list.*
import kotlinx.android.synthetic.main.fragment_onsaleitems.*


class CommentListFragment : Fragment() {
    var bundleList = Bundle()
    private lateinit var commentList : MutableLiveData<MutableList<Comment>>
    private lateinit var model : ItemViewModel
    private lateinit var userVM : UserViewModel
    private lateinit var adapter : CommentAdapter
    private lateinit var searchView : SearchView
    private lateinit var userId : MutableLiveData<String>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        model = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        userVM = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)

        userId=userVM.getUserId()


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comment_list, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //get items from db
        commentList = userVM.getUserComments()
        //model.setIsMyItem(false)
        model.setIsSoldToMe(false)
        model.setIsMyItem(false)

        //get NavController
        var nav: NavigationView = requireActivity().findViewById(R.id.nav_view)
        nav.setCheckedItem(R.id.commentListFragment)

        //set FAB
        var fab: FloatingActionButton = requireActivity().findViewById(R.id.fab)
        fab.hide()


        var fab2: ExtendedFloatingActionButton = requireActivity().findViewById(R.id.fab2)
        fab2.hide()

        //toolbar
        requireActivity().toolbar.setTitle(R.string.title_comment)
        var toolbar2 : Toolbar =  requireActivity().findViewById(R.id.toolbar2)
        toolbar2.visibility = View.GONE

        // Up navigation
        requireActivity().toolbar.setNavigationOnClickListener {
            val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
            drawerLayout.openDrawer(GravityCompat.START)
        }

        commentList.observe(viewLifecycleOwner, Observer{
                ia ->
            if (commentList.value!!.isEmpty()){
                empty_comment.visibility = TextView.VISIBLE
            } else {
                empty_comment.visibility = TextView.INVISIBLE

            }
            var recyclerView : RecyclerView = requireActivity().findViewById(R.id.recyclerComments)
            adapter = CommentAdapter(commentList.value!!,requireActivity() as AppCompatActivity)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(context)
        })

        // Up navigation
        requireActivity().toolbar.setNavigationOnClickListener {
            findNavController().navigate( R.id.userShowFragment )
        }


        // back navigation
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().navigate( R.id.userShowFragment )
                }
            })

    }

}
