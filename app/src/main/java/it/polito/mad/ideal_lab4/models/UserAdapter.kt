package it.polito.mad.ideal_lab4.models

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import it.polito.mad.ideal_lab4.MainActivity
import it.polito.mad.ideal_lab4.R
import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel

class UserAdapter(var data: MutableList<ProfileData>, var act : AppCompatActivity) : RecyclerView.Adapter<UserAdapter.UserViewHolder>(){

    var userVM : UserViewModel =ViewModelProvider(act).get(UserViewModel::class.java)

    override fun getItemCount() = data.size

    fun sendAllNotifications(userId : String, title : String){
        for(i : ProfileData in data){
            if(i.id != userId) {
                (act as MainActivity).sendMyNotification(i.id, act.resources.getString(R.string.`object`)
                        + " " + title
                        + " " + act.resources.getString(R.string.notificaItemSoldNO) , act.resources.getString(R.string.itemNoAvailableSold))
            }


        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.user_small, parent,false)
        return UserViewHolder(v, act, this)
    }


    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val u = data[position]
        holder.bind(u)
    }


    class UserViewHolder(v: View, act: AppCompatActivity, adapter : UserAdapter): RecyclerView.ViewHolder(v){
        val photo : ImageView = v.findViewById(R.id.photo)
        val fullName : TextView = v.findViewById(R.id.fullName)
        val card : CardView = v.findViewById(R.id.card)

        val progressbar : ProgressBar = v.findViewById(R.id.progress_circular)
        val mNav = act.findNavController(R.id.nav_host_fragment)
        val b: Bundle = Bundle()
        val act = act
        var userVM : UserViewModel =ViewModelProvider(act).get(UserViewModel::class.java)
        var itemVM : ItemViewModel =ViewModelProvider(act).get(ItemViewModel::class.java)
        var view = v
        val sellBtn : Button = v.findViewById(R.id.soldBtn)
        var adapter = adapter


        fun bind(a : ProfileData){
            if(a.fullName != "")
                fullName.text = a.fullName.toString()
            else{
                fullName.text = act.resources.getString(R.string.name)
                val spannable: Spannable = SpannableString(fullName.text)
                spannable.setSpan(
                    ForegroundColorSpan(ContextCompat.getColor(act.applicationContext, R.color.grayDisabled)),
                    0,
                    fullName.text.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                fullName.setText(spannable, TextView.BufferType.SPANNABLE)
            }


            if(itemVM.getCurItem().value!!.sold || !itemVM.getCurItem().value!!.available){
                sellBtn.isEnabled=false
            }

            (act as MainActivity).getProfileGlide( photo, a, a.id, progressbar, R.drawable.ic_user_icon)


            card.setOnClickListener {
                userVM.setOtherUser(true)
                userVM.setLastVisited(a)
                userVM.setLastFragment2("InterestedUsers")
                mNav.navigate(R.id.action_interestedUsersFragment_to_userShowFragment)
            }

            sellBtn.setOnClickListener {
                if (ContextCompat.checkSelfPermission(
                        act.applicationContext,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        act,
                        arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ),
                        0
                    )
                } else {
                    val builder = AlertDialog.Builder(act)
                    with(builder)
                    {
                        setTitle(act.getString(R.string.interested_confirm1))
                        //setMessage(act.getString(R.string.interested_confirm2))

                        setPositiveButton(

                            act.resources.getString(R.string.interested_yes),
                            DialogInterface.OnClickListener { _, _ ->
                                var userName_notification: String = act.resources.getString(R.string.undefined_username)
                                if(userVM.getFullname()!=""){
                                    userName_notification = userVM.getFullname()
                                }
                                (act as MainActivity).sendMyNotification(
                                    a.id,
                                    userName_notification + " " + act.resources.getString(R.string.notificationItemSoldOK) + " " + itemVM.getCurItem().value!!.title,
                                    act.resources.getString(R.string.itemAcquired)
                                )
                                adapter.sendAllNotifications(
                                    a.id,
                                    itemVM.getCurItem().value!!.title
                                )
                                (act as MainActivity).setItemBought(a.id, itemVM.getCurItem().value!!)


                                var item = itemVM.getCurItem()
                                item.value!!.sold = true
                                itemVM.addItem(item.value!!, userVM.getUserId().value!!)
                                mNav.navigate(R.id.action_interestedUsersFragment_to_itemDetailsFragment)
                            })
                        setNegativeButton(
                            act.resources.getString(R.string.interested_no),
                            DialogInterface.OnClickListener { _, _ -> })
                        show()
                    }
                }
            }
        }
    }

}