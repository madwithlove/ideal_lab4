package it.polito.mad.ideal_lab4.ui.userinfo

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RatingBar
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel

import it.polito.mad.ideal_lab4.R
import it.polito.mad.ideal_lab4.models.AdvItem
import it.polito.mad.ideal_lab4.models.Comment
import it.polito.mad.ideal_lab4.models.ProfileData
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_item_edit.*
import kotlinx.android.synthetic.main.fragment_user_rate.view.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

class UserRateFragment : Fragment() {
    private lateinit var model : ItemViewModel
    private lateinit var userVM : UserViewModel
    private var myRating = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        model = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        userVM = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)

        return inflater.inflate(R.layout.fragment_user_rate, container, false)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var fab2: ExtendedFloatingActionButton = requireActivity().findViewById(R.id.fab2)
        fab2.hide()
        var fab: FloatingActionButton = requireActivity().findViewById(R.id.fab)
        fab.hide()

        requireActivity().toolbar.setTitle(R.string.title_rate)


        var ownerProf : ProfileData = userVM.getLastVisited().value!!
        var ownerText : TextView = requireActivity().findViewById(R.id.ownerName)
        ownerText.setText(ownerProf.fullName)


        var ratingBar: RatingBar = requireActivity().findViewById(R.id.ratingBar)
        ratingBar.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            if(rating.toInt()<1) {
                ratingBar.rating = 1F
                myRating=1
            }
            else
                myRating = rating.toInt()
        }




        var comment : EditText =requireActivity().findViewById(R.id.comment)


        // Up navigation
        requireActivity().toolbar.setNavigationOnClickListener {
            findNavController().navigate(R.id.onSaleListFragment)
        }

        // Back navigation
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().navigate(R.id.onSaleListFragment)
                }
            }
        )
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.save, menu)
        super.onCreateOptionsMenu(menu, inflater)
        return
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.save -> {
                val builder = AlertDialog.Builder(requireContext())
                with(builder)
                {
                    setTitle(R.string.ratingSendConfirm)


                    setPositiveButton(
                        requireActivity().resources.getString(R.string.sendRating),
                        DialogInterface.OnClickListener { _, _ ->
                            Snackbar
                                .make(requireActivity()
                                    .findViewById(R.id.drawer_layout), requireActivity()
                                    .resources.getString(R.string.rating_sent), Snackbar.LENGTH_LONG)
                                .show()

                            var thisItem = model.getCurItem().value
                            var comment : EditText =requireActivity().findViewById(R.id.comment)

                            thisItem!!.comment=comment.getText().toString()
                            thisItem.rating=myRating

                            var ownerProf : ProfileData = userVM.getLastVisited().value!!

                            model.addItem( thisItem, ownerProf.id)
                            userVM.setItemBought(userVM.getUserId().value!!, thisItem)

                            var myComment : Comment = Comment( userVM.getFullname(),
                            Calendar.getInstance().time, myRating, comment.text.toString(), userVM.getUserId().value!!)

                            userVM.addComment(myComment, thisItem.uuid, ownerProf.id)



                            findNavController().navigate(R.id.onSaleListFragment)
                        })
                    setNegativeButton(
                        requireActivity().resources.getString(R.string.interested_no),
                        DialogInterface.OnClickListener { _, _ -> })
                    show()
                }
                true
            }
            else -> true
        }
    }


}




