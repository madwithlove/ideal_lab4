package it.polito.mad.ideal_lab4.ViewModels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.GeoPoint
import it.polito.mad.ideal_lab4.models.AdvItem
import it.polito.mad.ideal_lab4.models.ItemRepository
import it.polito.mad.ideal_lab4.models.ProfileData
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class ItemViewModel : ViewModel(){
    val repo = ItemRepository()
    var currentItem = MutableLiveData<AdvItem>()
    var items = MutableLiveData<MutableList<AdvItem>>(mutableListOf())
    var isMyItem = MutableLiveData<Boolean>(false)
    var isSoldToMe = MutableLiveData<Boolean>(false)
    var selectedSearch : String = "null"
    private var position : LatLng? = null
    private var positionAddr : String? = null

    init{
        items=repo.observableItems
        currentItem=repo.obsItem
    }

    fun getselectedSearch():String{
        return selectedSearch
    }

    fun setselectedSearch(value : String){
        selectedSearch=value
    }

    fun setPosition(p : LatLng?){
        this.position=p
    }

    fun setPositionAddr(p : String?){
        this.positionAddr=p
    }

    fun getPosition():LatLng?{
        return position
    }

    fun getPositionAddr():String?{
        return positionAddr
    }


    fun setItemPosition(pos : LatLng, addr: String){
        this.position = pos
        this.positionAddr = addr
        //currentItem.value!!.geopoint = GeoPoint(pos.latitude, pos.longitude)
        //currentItem.value!!.location = addr
        //addItem(currentItem.value!!, currentItem.value!!.creator_email)

    }

    fun getallItems(userId : String) : MutableLiveData<MutableList<AdvItem>>{
        getItemsFromDB(userId)
        return items
    }

    fun addItem(item : AdvItem, userId : String) {
        MainScope().launch {
            repo.addItem(item, userId)
        }
    }

    fun setIsMyItem(value : Boolean){
        isMyItem.value=value
    }

    fun getIsMyItem() : Boolean{
        return isMyItem.value!!
    }

    fun setIsSoldToMe(value : Boolean){
        isSoldToMe.value=value
    }

    fun getIsSoldToMe() : Boolean{
        return isSoldToMe.value!!
    }

    fun setCurrentItem(item : AdvItem){
        currentItem.value = item
    }

    fun getCurItem() : MutableLiveData<AdvItem>{
        loadCurItem()
        return currentItem
    }

    fun loadCurItem(){
        val job = viewModelScope.launch {
            val result = repo.getCurItem(currentItem.value!!.uuid)
            currentItem.value = result
        }
    }

    fun getItemsFromDB(userId : String)  {
        items.value!!.clear()
        val job = viewModelScope.launch {
            val result = repo.getAllItems(userId)
            items.value = result
        }
    }

    fun deleteItemFromDB(userId:String) {
        //repo.deleteItemFromDB(userId, currentItem.value!!.uuid)
    }



    fun addItemPreference(userId:String, user:ProfileData, item:AdvItem) {
        MainScope().launch {
            repo.addItemPreference(userId!!,user!!, item)
        }
    }


}
