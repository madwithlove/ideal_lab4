package it.polito.mad.ideal_lab4.models

import java.util.*

class Comment constructor()
{
    var creator : String = "null"
    var creatorEmail : String = "null"
    var date : Date = Calendar.getInstance().time
    var rating : Int = 0
    var comment : String = "null"

    constructor(creator : String,
                date : Date,
                rating : Int,
                comment : String,
                creatorEmail : String) : this() {
        this.creator = creator
        this.date=date
        this.rating=rating
        this.comment=comment
        this.creatorEmail = creatorEmail

    }


}
