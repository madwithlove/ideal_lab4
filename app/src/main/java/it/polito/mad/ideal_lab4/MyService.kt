package it.polito.mad.ideal_lab4

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel


class MyService : FirebaseMessagingService(){

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
                // Check if message contains a data payload.
                if (remoteMessage.data.size > 0 || remoteMessage.notification != null) {

                    val intent = Intent(this, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    val pendingIntent = PendingIntent.getActivity(this, 0, intent,
                        PendingIntent.FLAG_ONE_SHOT)

                    val name = "nome_channel"
                    val descriptionText = "desc_channel"
                    val importance = NotificationManager.IMPORTANCE_DEFAULT
                    val mChannel = NotificationChannel("asd", name, importance)
                    mChannel.description = descriptionText
                    // Register the channel with the system; you can't change the importance
                    // or other notification behaviors after this
                    val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                    notificationManager.createNotificationChannel(mChannel)

                    val notificationBuilder = NotificationCompat.Builder(this, mChannel.id)
                        .setSmallIcon( R.mipmap.ic_launcher )
                        .setContentTitle(remoteMessage.data["title"])
                        .setContentText(remoteMessage.data["message"])
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)

                   // val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    notificationManager.notify(0, notificationBuilder.build())
                }
    }

}