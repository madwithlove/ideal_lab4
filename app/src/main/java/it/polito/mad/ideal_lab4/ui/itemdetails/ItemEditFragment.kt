package it.polito.mad.ideal_lab4.ui.itemdetails

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.*
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.GeoPoint
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import it.polito.mad.ideal_lab4.GpsUtils
import it.polito.mad.ideal_lab4.MainActivity
import it.polito.mad.ideal_lab4.R
import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel
import it.polito.mad.ideal_lab4.models.AdvItem
import it.polito.mad.ideal_lab4.models.ProfileData
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import kotlinx.android.synthetic.main.fragment_item_edit.*
import kotlinx.android.synthetic.main.fragment_item_edit.date
import kotlinx.android.synthetic.main.fragment_item_edit.description
import kotlinx.android.synthetic.main.fragment_item_edit.location
import kotlinx.android.synthetic.main.fragment_item_edit.photo
import kotlinx.android.synthetic.main.fragment_item_edit.price
import kotlinx.android.synthetic.main.fragment_item_edit.title
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class ItemEditFragment : Fragment(), AdapterView.OnItemSelectedListener{
    lateinit var mCurrentPhotoPath : String
    private var photoURI : Uri? = null
    private val IMAGE_PICK_CODE = 1000
    val CAPTURE_IMAGE_REQUEST = 1
    val REQUEST_IMAGE_CAPTURE = 1
    private var fromFAB : Boolean = true
    private lateinit var model : ItemViewModel
    private lateinit var userVM : UserViewModel
    var item = MutableLiveData<AdvItem>()
    private lateinit var interestedUsers : MutableLiveData<MutableList<ProfileData>>
    private lateinit var previousFrag : String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)     /*  gestisce l'Option menu  */
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        model = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        userVM = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        return inflater.inflate(R.layout.fragment_item_edit, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Get previous fragment
        previousFrag = userVM.getLastFragmentEdit()

        requireActivity().toolbar.setTitle(R.string.title_item_edit)
        var toolbar2 : Toolbar =  requireActivity().findViewById(R.id.toolbar2)
        toolbar2.visibility=View.GONE



        val nav: NavigationView = requireActivity().findViewById(R.id.nav_view)
        nav.menu.findItem(R.id.userShowFragment).setChecked(false)
        nav.menu.findItem(R.id.itemListFragment).setChecked(false)

        val fab: FloatingActionButton = requireActivity().findViewById(R.id.fab)
        fab.hide()
        val fab2: ExtendedFloatingActionButton = requireActivity().findViewById(R.id.fab2)
        fab2.hide()

        //mappa
        location.setOnClickListener{
            GpsUtils(requireContext()).turnGPSOn(object : GpsUtils.OnGpsListener {
                override fun gpsStatus(isGPSEnable: Boolean) {
                    // turn on GPS
                    var isGPS = isGPSEnable
                    var b = Bundle()
                    b.putString("type", "Item")
                    findNavController().navigate( R.id.googleMapsFragment,b)
                }
            }, 3003)
        }

        item = model.getCurItem()


        //prendo utenti interessati
        interestedUsers= userVM.getInterestedUsers(item.value!!.uuid)

        //setta la UI con i dati dell'item
        setAdvItem(item.value!!)

        registerForContextMenu(imageButton)
        imageButton.setOnClickListener { it.showContextMenu() }

        // back navigation
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    model.setPosition(null)
                    model.setPositionAddr(null)
                    when(previousFrag){
                        "item_list" -> {
                            findNavController().navigate(R.id.action_itemEditFragment_to_itemListFragment)
                        }
                        "item_details" -> {
                            findNavController().navigate(R.id.action_itemEditFragment_to_itemDetailsFragment)
                        }
                    }
                }
            }
        )

        // up navigation
        requireActivity().toolbar.setNavigationOnClickListener {
            model.setPosition(null)
            model.setPositionAddr(null)
            when(previousFrag){
                "item_list" -> {
                    findNavController().navigate(R.id.action_itemEditFragment_to_itemListFragment)
                }
                "item_details" -> {
                    findNavController().navigate(R.id.action_itemEditFragment_to_itemDetailsFragment)
                }
            }
        }

        //setting date listener
        date.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(requireContext(), DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                val currentDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                val currentMonth = Calendar.getInstance().get(Calendar.MONTH)
                val currentYear = Calendar.getInstance().get(Calendar.YEAR)
                val str : String = "$currentYear-$currentMonth-$currentDay"
                val selectedDate : String = "$year-$month-$dayOfMonth"

                val todayDate : Date = sdf.parse(str)!!
                val selDate : Date = sdf.parse(selectedDate)!!

                if(selDate.before(todayDate)){
                    Toast.makeText(requireContext(),requireActivity().resources.getString(R.string.time_error),Toast.LENGTH_SHORT).show()
                }
                else{
                    // Display Selected date in
                    var mm = month
                    mm++
                    date.setText("${dayOfMonth}/${mm}/${year}")
                    item.value!!.date="${dayOfMonth}/${mm}/${year}"
                }
            }, year, month, day)
            dpd.show()
        }
        manageSpinner()
    }

    private fun manageSpinner() {

        val spinner: Spinner = requireActivity().findViewById(R.id.categ)

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.category_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
        when (item.value!!.category) {
            requireActivity().resources.getString(R.string.c1) -> spinner.setSelection(0)
            requireActivity().resources.getString(R.string.c2) -> spinner.setSelection(1)
            requireActivity().resources.getString(R.string.c3) -> spinner.setSelection(2)
            requireActivity().resources.getString(R.string.c4) -> spinner.setSelection(3)
            requireActivity().resources.getString(R.string.c5) -> spinner.setSelection(4)
            requireActivity().resources.getString(R.string.c6) -> spinner.setSelection(5)
            requireActivity().resources.getString(R.string.c7) -> spinner.setSelection(6)
            requireActivity().resources.getString(R.string.c8) -> spinner.setSelection(7)
            else -> spinner.setSelection(0)
        }
        spinner.onItemSelectedListener = this
        sub_categ.onItemSelectedListener = this
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        var cat : Spinner = p0 as Spinner
        if(cat.id == R.id.categ){
            //sei nel primo spinner
            val spinner2: Spinner = requireActivity().findViewById(R.id.sub_categ)
            var id: Int = 0
            when (p2) {
                0 -> id = R.array.sub1
                1 -> id = R.array.sub2
                2 -> id = R.array.sub3
                3 -> id = R.array.sub4
                4 -> id = R.array.sub5
                5 -> id = R.array.sub6
                6 -> id = R.array.sub7
                7 -> id = R.array.sub8
                else -> id = R.array.sub1
            }
            item.value!!.category = p2.toString()

            ArrayAdapter.createFromResource(
                requireContext(),
                id,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Apply the adapter to the spinner
                spinner2.adapter = adapter
                val total = spinner2.adapter.count
                for(i in 0 until total ){
                    if(spinner2.adapter.getItem(i).toString() == item.value!!.subcategory) {
                        spinner2.setSelection(i)
                        break
                    }
                }
            }
        }
        if(cat.id == R.id.sub_categ){
            //sei nel secondo spinner
            item.value!!.subcategory = p2.toString()
        }

    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    private fun setAdvItem(item: AdvItem){
        if(item.title != requireActivity().resources.getString(R.string.title))
            this.title.setText( item.title )
        if(item.description != requireActivity().resources.getString(R.string.description))
            description.setText( item.description )

        if (model.getPositionAddr() != null){
            location.setText(model.getPositionAddr())
        }
        else{
            if(item.location != requireActivity().resources.getString(R.string.location))
                location.setText(item.location)
        }
/*
        if(item.location != requireActivity().resources.getString(R.string.location))
            location.setText( item.location )
*/


        date.setText( item.date.toString() )
        if(item.price != 0.00) {
            this.price.setText("%.2f".format(item.price))
            if(this.price.text.contains(","))
                this.price.setText(this.price.text.toString().replace(",","."))
        }

        (requireActivity() as MainActivity).getImgGlide( photo, item, editprogressbar, R.drawable.ic_empty_photo )

        availability.setChecked(item.available)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.save, menu)
        super.onCreateOptionsMenu(menu, inflater)
        return
    }

    private fun sendNotificationItemBlocked(itemId: String, title:String){
        for(user in interestedUsers.value!!){
            (requireActivity() as MainActivity).sendMyNotification(user.id, requireActivity().resources.getString(R.string.`object`)+" "+item.value!!.title+" "+
                    requireActivity().resources.getString(R.string.notificaItemAvailableNO), requireActivity().resources.getString(R.string.itemNoAvailable))
        }
    }

    private fun sendNotificationItemUnlocked(itemId: String, title:String){
        for(user in interestedUsers.value!!){
            (requireActivity() as MainActivity).sendMyNotification(user.id, requireActivity().resources.getString(R.string.`object`)+" "+item.value!!.title+" "+
                    requireActivity().resources.getString(R.string.notificaItemAvailableYES), requireActivity().resources.getString(R.string.itemAgainAvailable))
        }
    }

    override fun onOptionsItemSelected(itemm: MenuItem): Boolean {
        return when(itemm.itemId){
            R.id.save -> {
                if(title.text.toString().length == 0)
                    this.item.value!!.title = requireActivity().resources.getString(R.string.title)
                else
                    this.item.value!!.title = title.text.toString()


                if(description.text.toString().length == 0)
                    this.item.value!!.description = requireActivity().resources.getString(R.string.description)
                else
                    this.item.value!!.description = description.text.toString()

                var priceDouble = price.text.toString()
                if(priceDouble.length == 0)
                {
                    var zero = "0.00"
                    this.item.value!!.price = zero.toDouble()
                }
                else {
                    priceDouble = priceDouble.replace(',', '.')
                    this.item.value!!.price = priceDouble.toDouble()
                }

                if(location.text.toString().length == 0)
                    this.item.value!!.location = requireActivity().resources.getString(R.string.location)
                else
                    this.item.value!!.location = location.text.toString()

                this.item.value!!.creator_email = userVM.getUserId().value!!
                this.item.value!!.creator_fullname = userVM.getFullname()

                if(availability.isChecked != item.value!!.available && previousFrag=="item_details"){
                    val builder = AlertDialog.Builder(requireContext())
                    with(builder)
                    {
                        if(availability.isChecked) {
                            setTitle(R.string.unlock)
                            sendNotificationItemUnlocked(item.value!!.uuid, item.value!!.title)
                        }
                        else
                        {
                            setTitle(R.string.lock)
                            sendNotificationItemBlocked(item.value!!.uuid, item.value!!.title)
                        }
                        setMessage(R.string.lock_notify)

                        setPositiveButton(
                            requireActivity().resources.getString(R.string.interested_yes),
                            DialogInterface.OnClickListener { _, _ ->
                                item.value!!.available = availability.isChecked
                                model.addItem(item.value!!, userVM.getUserId().value!!)


                                Snackbar
                                    .make(requireActivity()
                                        .findViewById(R.id.drawer_layout), requireActivity()
                                        .resources.getString(R.string.saveComplete), Snackbar.LENGTH_LONG)
                                    .show()
                                if(model.getPosition() != null)
                                    item.value!!.geopoint= GeoPoint(model.getPosition()!!.latitude, model.getPosition()!!.longitude)
                                model.setPosition(null)
                                model.setPositionAddr(null)
                                findNavController().navigate(R.id.itemDetailsFragment)
                            })
                        setNegativeButton(
                            requireActivity().resources.getString(R.string.interested_no),
                            DialogInterface.OnClickListener { _, _ -> })
                        show()
                    }
                }
                else{
                    item.value!!.available = availability.isChecked
                    model.addItem(item.value!!, userVM.getUserId().value!!)
                    if(model.getPosition() != null)
                        item.value!!.geopoint= GeoPoint(model.getPosition()!!.latitude, model.getPosition()!!.longitude)
                    model.setPosition(null)
                    model.setPositionAddr(null)
                    findNavController().navigate(R.id.itemDetailsFragment)
                }
                true

            }
            else -> super.onOptionsItemSelected(itemm)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val type  = object : TypeToken<AdvItem>() {}.type
        val gsonData = Gson().toJson(item.value,type)
        val b = Bundle()
        b.putString("item", gsonData)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if(savedInstanceState != null){
            val data = savedInstanceState.getString("item","null")
            if(data != "null"){
                val type = object : TypeToken<AdvItem>() {}.type
                item.value = Gson().fromJson(data, type)
            }
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        activity?.menuInflater?.inflate(R.menu.menu_image, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.gallery -> {
                openGallery()
                true
            }
            R.id.camera -> {
                captureImage()
                true
            }
            else ->
                super.onContextItemSelected(item)
        }
    }

    private fun captureImage() {

        if (ContextCompat.checkSelfPermission(requireActivity().applicationContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        } else {

            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(requireActivity().packageManager) != null) {
                // Create the File where the photo should go
                try {
                    //Creates a blank image file
                    var photoFile = createImageFile()

                    // Continue only if the File was successfully created
                    photoURI =
                        FileProvider.getUriForFile(requireActivity().applicationContext,
                            "it.polito.mad.ideal_lab4.fileprovider",
                            photoFile
                        )
                    //saving current photo_uri
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, CAPTURE_IMAGE_REQUEST)
                } catch (ex: Exception) {
                    // Error occurred while creating the File
                    displayMessage(requireActivity().baseContext,"Capture Image Bug: "  + ex.message.toString())
                }
            } else {
                displayMessage(requireActivity().baseContext, "Null")
            }
        }
    }

    private fun displayMessage(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".png",     /* suffix */
            storageDir          /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    private fun openGallery() {
        if (ContextCompat.checkSelfPermission(requireActivity().applicationContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        }
        else {
            val intent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            intent.type = "image/*"
            startActivityForResult(intent, IMAGE_PICK_CODE)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAPTURE_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {  // image dalla camera
            item.value!!.photo_url = photoURI.toString()
            Glide.with(requireActivity()).load(Uri.parse(item.value!!.photo_url)).apply(RequestOptions.circleCropTransform()).into(photo)
        }
        else if(requestCode == IMAGE_PICK_CODE && resultCode == Activity.RESULT_OK) {  // image dalla gallery
            item.value!!.photo_url = data?.data.toString()
            Glide.with(requireActivity()).load(Uri.parse(item.value!!.photo_url)).apply(RequestOptions.circleCropTransform()).into(photo)
        }
        else
            displayMessage(requireActivity().baseContext, getString(R.string.camera_error))
    }
}
