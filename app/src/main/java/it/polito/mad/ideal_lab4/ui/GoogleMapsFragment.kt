package it.polito.mad.ideal_lab4.ui

import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.adapters.ViewBindingAdapter.setPadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ComputableLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import it.polito.mad.ideal_lab4.R
import it.polito.mad.ideal_lab4.ViewModels.ItemViewModel
import it.polito.mad.ideal_lab4.ViewModels.UserViewModel
import kotlinx.android.synthetic.main.app_bar_main.*
import java.io.IOException

class GoogleMapsFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var mSearchText : EditText
    private lateinit var type : String
    private var tempPos : LatLng? = null
    private var tempAddr : String? = null
    private lateinit var userVM : UserViewModel
    private lateinit var itemVM : ItemViewModel
    private var markers : MutableList<Marker> = mutableListOf()
    private lateinit var currentPos : LatLng
    private var myUserPos : LatLng? = null
    private var myUserPos_isPlaced: Boolean = false
    private lateinit var otherPos : LatLng
    private var isItemZoomed : Boolean = true
    private var line : Polyline? = null
    private var mySavedInstanceState: Bundle? = null

    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_google_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(line != null)
            line!!.remove()


        //Get type of object to modify (user or item)
        type = arguments?.getString("type","null") ?: "null"
        itemVM = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        userVM = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
        // Load map
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.googleMapsFragment) as SupportMapFragment
        mapFragment.setRetainInstance(true)
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        //set FABs
        var fab: FloatingActionButton = requireActivity().findViewById(R.id.fab)
        fab.hide()
        var fab2: ExtendedFloatingActionButton = requireActivity().findViewById(R.id.fab2)
        fab2.hide()
        var fab_map: FloatingActionButton = requireActivity().findViewById(R.id.fab_map)
        if(type=="GET_Profile" || type == "GET_Item") {
            fab_map.show()

            fab_map.setOnClickListener {
                if (isItemZoomed) {
                    // Set zoom on user-item
                    if (myUserPos != null) {
                        isItemZoomed = false
                        line = map.addPolyline(
                            PolylineOptions().add(
                                myUserPos, otherPos
                            ).width(9F).color(
                                ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark)
                            ).geodesic(false)
                        )
                        zoomMultipleMarkers()
                        fab_map.setImageResource(R.drawable.ic_zoom_itemonly)
                    } else {
                        //cerca la posizione corrente
                        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                            if (location != null) {
                                lastLocation = location
                                val currentLatLng = LatLng(location.latitude, location.longitude)
                                markers.add(
                                    map.addMarker(
                                        MarkerOptions().position(currentLatLng)
                                            .title(getAddress(currentLatLng))
                                    )
                                )

                                var builder = LatLngBounds.Builder()

                                for (marker in markers) {
                                    builder.include(marker.getPosition())
                                }
                                myUserPos = currentLatLng
                            }
                        }
                    }
                } else {
                    // Set zoom on item only
                    isItemZoomed = true
                    line!!.remove()
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(otherPos, 12.0f))
                    fab_map.setImageResource(R.drawable.ic_zoom_distance)
                }
            }
        }

        //toolbar
        requireActivity().toolbar.setTitle(R.string.title_maps_position)
        var toolbar2 : Toolbar =  requireActivity().findViewById(R.id.toolbar2)
        toolbar2.visibility = View.GONE

        requireActivity().toolbar.setNavigationOnClickListener {
            fab_map.hide()
            fab_map.setImageResource(R.drawable.ic_zoom_distance)
            when(type){
                "Profile" -> findNavController().navigate(R.id.userEditorFragment)      // From edit
                "Item" -> findNavController().navigate(R.id.itemEditFragment)
                "GET_Profile" -> findNavController().navigate(R.id.userShowFragment)    // From details
                "GET_Item" -> findNavController().navigate(R.id.itemDetailsFragment)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    fab_map.hide()
                    fab_map.setImageResource(R.drawable.ic_zoom_distance)
                    when(type){
                        "Profile" -> findNavController().navigate(R.id.userEditorFragment)
                        "Item" -> findNavController().navigate(R.id.itemEditFragment)
                        "GET_Profile" -> findNavController().navigate(R.id.userShowFragment)
                        "GET_Item" -> findNavController().navigate(R.id.itemDetailsFragment)
                    }
                }
            }
        )
    }

    override fun onPause() {
        super.onPause()
        if(line != null)
            line!!.remove()
    }


    private fun confirmPosition(){
        if(type=="Profile"){
            //userVM = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
            if(tempPos != null && tempAddr != null){
                //fab_map.hide()
                userVM.setUserPosition(tempPos!!, tempAddr!!)
                findNavController().navigate(R.id.userEditorFragment)
            }
            else
                Toast.makeText(requireContext(),requireActivity().resources.getString(R.string.choose_position), Toast.LENGTH_SHORT).show()

        }
        else if(type=="Item"){
           // itemVM = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
            if(tempPos != null && tempAddr != null){
                //fab_map.hide()
                itemVM.setItemPosition(tempPos!!, tempAddr!!)
                findNavController().navigate(R.id.itemEditFragment)
            }

            else
                Toast.makeText(requireContext(),requireActivity().resources.getString(R.string.choose_position), Toast.LENGTH_SHORT).show()
        }

    }

    //cerca la posizione in base all'input testuale
    private fun geoLocate(){
        var searchString : String = mSearchText.text.toString()

        // controlla il requirecontext
        var geocoder : Geocoder = Geocoder(this.context)
        var list : List<Address> = ArrayList()
        try{
            list = geocoder.getFromLocationName(searchString, 1)
        }
        catch(e: IOException) {

        }
        if(list.size>0) {
            var address: Address = list.get(0)
            val latlng = LatLng(address.latitude, address.longitude)

            markers.add(map.addMarker(MarkerOptions().position(latlng).title(address.toString())))

            map.moveCamera(CameraUpdateFactory.newLatLng(latlng))
            map.getUiSettings().setZoomControlsEnabled(true)
            map.setOnMarkerClickListener(this)
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 6.0f))

            // set the retrieved location
            tempPos=latlng
            tempAddr = getAddress(tempPos!!)
            mSearchText.setText(tempAddr.toString())

        }
    }

    private fun init(){
        // initialize widgets
        mSearchText = requireActivity().findViewById(R.id.input_search)

        mSearchText.setOnEditorActionListener(
            TextView.OnEditorActionListener { p0, actionId, keyEvent ->
                if(actionId== EditorInfo.IME_ACTION_SEARCH
                    || actionId == EditorInfo.IME_ACTION_DONE
                    || keyEvent!!.action == KeyEvent.ACTION_DOWN
                    || keyEvent.action == KeyEvent.KEYCODE_ENTER)
                    geoLocate()
                false

            })
    }

    private fun writeCurrentLocation(){
        var currentAddr = getAddress(currentPos)
        mSearchText.setText(currentAddr)
        tempPos = currentPos
        tempAddr = currentAddr
        
    }

    private fun setObjectPosition(){
        var search_layout = requireActivity().findViewById<RelativeLayout>(R.id.relLayout1)
        var pos = LatLng(0.0, 0.0)
        if(type=="GET_Item" || type=="GET_Profile") {
            search_layout.visibility= View.GONE

            if(type == "GET_Item"){
                //itemVM = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
                pos = LatLng(itemVM.getCurItem().value!!.geopoint.latitude, itemVM.getCurItem().value!!.geopoint.longitude)
            }
            else if(type=="GET_Profile"){
                //var userVM = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)
                var isMyProfile = !userVM.getIsOtherUser().value!!
                if(isMyProfile){
                    pos = LatLng(userVM.getUserProfile().value!!.pos.latitude, userVM.getUserProfile().value!!.pos.longitude)
                }
                else{
                    pos = LatLng(userVM.getLastVisited().value!!.pos.latitude, userVM.getLastVisited().value!!.pos.longitude)
                }
            }
            otherPos = pos
            placeMarkerOnMap(pos)


            currentPos = LatLng(0.0,0.0)
            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                if (location != null) {
                    lastLocation = location
                    val currentLatLng = LatLng(location.latitude, location.longitude)
                    if(!myUserPos_isPlaced){
                        myUserPos_isPlaced = true
                        markers.add(map.addMarker(MarkerOptions().position(currentLatLng).title(getAddress(currentLatLng))))
                    }

                    var builder = LatLngBounds.Builder()

                    for (marker in markers) {
                        builder.include(marker.getPosition())
                    }

                    currentPos = currentLatLng
                    myUserPos = currentLatLng

                }
            }
            zoomMultipleMarkers()
        }
        else{
            currentPos = LatLng(0.0,0.0)
            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                if (location != null) {
                    lastLocation = location
                    val currentLatLng = LatLng(location.latitude, location.longitude)
                    currentPos = currentLatLng
                }
            }
            search_layout.visibility= View.VISIBLE
            mSearchText = requireActivity().findViewById(R.id.input_search)
            init()
        }
    }


    override fun onMapReady(googleMap: GoogleMap?) {

        if (googleMap != null) {
            map = googleMap
        }
        map.getUiSettings().setZoomControlsEnabled(true)
        map.setOnMarkerClickListener(this)
        map.setPadding(0,180,0,0)
        setUpMap()
        setObjectPosition()

        googleMap?.setOnMyLocationButtonClickListener {
            if(myUserPos != null) {
                map.animateCamera(CameraUpdateFactory.newLatLng(myUserPos))
                if (type == "Profile" || type == "Item")
                    writeCurrentLocation()
            }
            else{
                Toast.makeText(requireContext(), R.string.turnon_gps, Toast.LENGTH_SHORT).show()
                fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                    if (location != null) {
                        lastLocation = location
                        val currentLatLng = LatLng(location.latitude, location.longitude)
                        markers.add(
                            map.addMarker(
                                MarkerOptions().position(currentLatLng)
                                    .title(getAddress(currentLatLng))))

                        var builder = LatLngBounds.Builder()

                        for (marker in markers) {
                            builder.include(marker.getPosition())
                        }
                        currentPos = currentLatLng
                        myUserPos = currentLatLng
                    }
                }
            }
            true
        }

        googleMap?.uiSettings?.isMyLocationButtonEnabled = true
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        return false
    }

    //posiziona il marker in base alla posizione corrente del device
    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }

        map.isMyLocationEnabled = true
    }

    private fun placeMarkerOnMap(location: LatLng) {
        val markerOptions = MarkerOptions().position(location)

        val titleStr = getAddress(location)
        tempAddr = titleStr
        markerOptions.title(titleStr)

        markers.add(map.addMarker(markerOptions))
    }

    //trova l'indirizzo in base alla posizione passata
    private fun getAddress(latLng: LatLng): String {
        val geocoder = Geocoder(this.context)
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            if (null != addresses && !addresses.isEmpty()) {
                address = addresses[0]
                addressText = address.getAddressLine(0)
            }
        } catch (e: IOException) {
            Log.e("MapsActivity", e.localizedMessage)
        }
        return addressText
    }

    fun zoomMultipleMarkers (){
        var builder = LatLngBounds.Builder()

        for (marker in markers) {
            builder.include(marker.getPosition())
        }
        var bounds = builder.build()

        var width : Int = requireActivity().getResources().getDisplayMetrics().widthPixels
        var height : Int = requireActivity().getResources().getDisplayMetrics().heightPixels
        //var padding = (width * 0.25f)   // offset from edges of the map 25% of screen
        var padding = (width*0.15f)

        var cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding.toInt())
        map.animateCamera(cu)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()

        if(type=="Item" || type=="Profile")
            inflater.inflate(R.menu.save, menu)

        super.onCreateOptionsMenu(menu, inflater)
        return
    }

    override fun onOptionsItemSelected(itemm: MenuItem): Boolean {
        return when(itemm.itemId){
            R.id.save -> {
                confirmPosition()
                true
            }
            else -> super.onOptionsItemSelected(itemm)
        }
    }

}