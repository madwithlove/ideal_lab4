package it.polito.mad.ideal_lab4.ViewModels

import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.GeoPoint
import it.polito.mad.ideal_lab4.models.AdvItem
import it.polito.mad.ideal_lab4.models.Comment
import it.polito.mad.ideal_lab4.models.ProfileData
import it.polito.mad.ideal_lab4.models.UserRepository
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class UserViewModel : ViewModel() {

    //INFO DELL'UTENTE
//    private val user: MutableLiveData<ProfileData> by lazy {
//        MutableLiveData<ProfileData>().also { loadUsers() } }
    private var user = MutableLiveData<ProfileData>()
    private var temporaryPhotoUri = MutableLiveData("null")
    private var userId : MutableLiveData<String> = MutableLiveData("null")
    private var userItems = MutableLiveData<MutableList<AdvItem>>(mutableListOf())
    private var userComments = MutableLiveData<MutableList<Comment>>(mutableListOf())
    private var userInterests = MutableLiveData<MutableList<AdvItem>>(mutableListOf())
    private var interestedUsers = MutableLiveData<MutableList<ProfileData>>(mutableListOf())
    private var topic : MutableLiveData<String> = MutableLiveData("null")
    private lateinit var defaultProfile: ProfileData
    private var fullname_drawer: MutableLiveData<String> = MutableLiveData("null")
    private var itemsBought = MutableLiveData<MutableList<AdvItem>>(mutableListOf())
    private var lastFragment : String = "Home"
    private var lastFragment2 : String = "ItemDetails"
    private var lastFragmentEditItem : String = "item_details"
    private var position : LatLng? = null
    private var positionAddr : String? = null


    //INFO DELL'ULTIMO UTENTE VISUALIZZATO

    //serve a distinguere se stiamo visualizzando il nostro profilo o quello di un altro utente
    //switcharla ogni volta che va cambiata la visualizzazione!!
    //è permanente, quindi una volta cambiata non c'è bisogno di farlo ulteriori volte, anche a fronte di rotazione schermo
    private val otherUser : MutableLiveData<Boolean> = MutableLiveData(false)
    //in caso otherUser == true, contiene l'id del profilo da visualizzare
    private val lastUserVisited : MutableLiveData<ProfileData> = MutableLiveData()

    val userRepo : UserRepository = UserRepository()

    init{
        user=userRepo.user
        interestedUsers=userRepo.interestedObservable
        userInterests=userRepo.observableItemsOfInterest
        itemsBought=userRepo.observableItemsBought
        userComments=userRepo.observableUserComments

    }

    fun addComment(comment : Comment, itemId:String, userId: String){
        MainScope().launch {
            userRepo.addComment(comment, userId, itemId)
        }
    }

    fun getLastFragment(): String{
        return lastFragment
    }

    fun setLastFragment(id : String){
        lastFragment = id
    }

    fun getLastFragment2(): String{
        return lastFragment2
    }

    fun setLastFragment2(id : String){
        lastFragment2 = id
    }

    fun getLastFragmentEdit(): String{
        return lastFragmentEditItem
    }

    fun setLastFragmentEdit(id : String){
        lastFragmentEditItem = id
    }


    fun getUserProfile() : MutableLiveData<ProfileData>
    {
        loadUsers()
        return user
    }

    fun getTemporaryPhotoUri() : MutableLiveData<String>{
        return temporaryPhotoUri
    }

    fun setTemporaryPhotoUri(profileData : String){
        temporaryPhotoUri.value=profileData
    }

    fun getUserInterests() : MutableLiveData<MutableList<AdvItem>>{
        loadUserInterests()
        return userInterests
    }

    fun getUserComments() : MutableLiveData<MutableList<Comment>>{
        loadUserComments()
        return userComments
    }

    fun getItemsBought(): MutableLiveData<MutableList<AdvItem>>{
        loadItemsBought()
        return itemsBought
    }

    private fun loadItemsBought(){
        viewModelScope.launch {
           // itemsBought.value!!.clear()
            var result = userRepo.getItemsBought(userId.value!!)
            if(result != null)
                itemsBought.value=result
            else{
                //errore in vm
            }

        }
    }

    fun setItemBought(userId: String, itemId:AdvItem){
        MainScope().launch{
            try {
                userRepo.setItemBought(userId, itemId)
            }catch (e: Exception){
                throw e
            }
        }
    }

    fun setPosition(p : LatLng?){
        this.position=p
    }

    fun setPositionAddr(p : String?){
        this.positionAddr=p
    }

    fun getPosition():LatLng?{
        return position
    }

    fun getPositionAddr():String?{
        return positionAddr
    }


    fun setUserPosition(pos : LatLng, addr : String){
        this.position = pos
        this.positionAddr = addr
        //this.user.value!!.pos = GeoPoint(pos.latitude, pos.longitude)
        //this.user.value!!.location = addr
        //setUser(this.user.value!!)

    }

    private fun loadUserInterests(){
        val job2 = viewModelScope.launch {
            //retrieve user interests
            val result = userRepo.getUserInterests(userId.value!!)
            if(result != null){
                userInterests.value = result
            }
//            else{
//
//            }
        }
    }

    private fun loadUserComments(){
        val job = viewModelScope.launch {
            //retrieve user interests
            var result: MutableList<Comment>?
            if(getIsOtherUser().value!!) {
                result = userRepo.getUserComments(lastUserVisited.value!!.id)
            } else {
                result = userRepo.getUserComments(userId.value!!)
            }
            if(result != null){
            userComments.value = result
            }
            else{
                //TO DO EXCEPTION
            }
        }
    }


    fun removeItemPreference(userId:String, user:ProfileData, item:AdvItem){
        MainScope().launch {
            userRepo.removeItemPreference(userId!!,user!!, item)
        }

    }

    fun setUser(user : ProfileData){
        this.user.value = user
        MainScope().launch {
            val u : UserRepository = UserRepository()
            u.updateUser(userId.value!!, user)
        }
    }

    fun getFullname() : String {
        return user.value!!.fullName
    }

    fun getNickname() : String {
        return user.value!!.nick
    }

    fun getFullname_for_drawer() : MutableLiveData<String> {
        return fullname_drawer
    }

    fun setOtherUser(bool : Boolean){
        otherUser.value = bool
    }
    fun getIsOtherUser() : MutableLiveData<Boolean>{
        return otherUser
    }


    fun setLastVisited(user : ProfileData){
        lastUserVisited.value = user
    }

    fun getLastVisited():MutableLiveData<ProfileData>{
        loadUsers()
        return lastUserVisited
    }

    fun setUserId(id: String){
        userId.value = id
    }

    fun createTopic() {
        var str: String? = userId.value
        if (!str.isNullOrEmpty()) {
            this.topic.value = "/topics/"+str.replace("@", "at", false)
        }
    }

    fun getTopic() :String {
        return topic.value?:""
    }

    fun getUserId() : MutableLiveData<String>{
        return userId
    }

    fun getPhoto(uri: Uri, id : String) {
        MainScope().launch {
            val u : UserRepository = UserRepository()
            u.loadPhoto(uri, id)
        }
    }

    fun getUserItems() : MutableLiveData<MutableList<AdvItem>>{
        loadUserItems()
        return userItems
    }

    private fun loadUserItems(){
        userItems.value!!.clear()
    viewModelScope.launch {
        userItems.value = userRepo.getUserItems(userId.value!!)
    }
    }

    fun getInterestedUsers(itemId : String) : MutableLiveData<MutableList<ProfileData>>{
        interestedUsers.value!!.clear()
        loadInterestedUsers(itemId)
        return interestedUsers

    }

    private fun loadInterestedUsers(itemId : String){

        MainScope().launch {
            interestedUsers.value = userRepo.getInterestedUsers(itemId, userId.value!!)
        }
    }



    private fun loadUsers() {
        var userRepo = UserRepository()
        if(otherUser.value!!){
            //Prendo le info dell'altro utente
            val job = viewModelScope.launch {
                val result = userRepo.getUser(lastUserVisited.value!!.id, defaultProfile)
                if(result != null) {
                    //caricamento riuscito
                    //user.value = result
                    //user.value!!.id=userId.value!!
                    lastUserVisited.value = result
                }
                else{
                    //il caricamento dal db è fallito!
                }

            }
        }
        else{
            //sono nel mio account
            if(userId.value.equals("null")){
                //non sono loggato con google!!
                user.value = ProfileData()
            }
            else{
                //sono loggato! posso caricare il mio profilo!
                val job = viewModelScope.launch {
                    val result = userRepo.getUser(userId.value!!, defaultProfile)
                    if(result != null) {
                        //caricamento riuscito
                        user.value = result
                        user.value!!.id=userId.value!!
                        fullname_drawer.value = user.value!!.fullName
                    }
                    else{
                        //il caricamento dal db è fallito!
                    }

                }
            }
        }
    }

    fun setDefaultProfile(defaultProfile: ProfileData){
        this.defaultProfile = defaultProfile
        this.defaultProfile.id = userId.value!!
    }

}